# **Algo 2 Resumen**

Resumen de [75.41]Algoritmos y Programación 2 Catedra Mendez.

> - TODO:
>   - Mejorar Ejemplos de Arbol B
>   - Equacion de recurrencia Quicksort

</br>

- [**Algo 2 Resumen**](#algo-2-resumen)
  - [**Estructura**](#estructura)
  - [**Memoria**](#memoria)
    - [**Memoria Dinámica**](#memoria-dinámica)
    - [**Funciones de memoria en C:**](#funciones-de-memoria-en-c)
      - [`puntero = malloc(tamanio_de_memoria);`](#puntero--malloctamanio_de_memoria)
      - [`puntero = calloc(tamanio_vector, tamanio_de_cada_posicion);`](#puntero--calloctamanio_vector-tamanio_de_cada_posicion)
      - [`puntero = realloc(puntero, nuevo_tamanio);`](#puntero--reallocpuntero-nuevo_tamanio)
      - [`free(puntero);`](#freepuntero)
      - [`sizeof(tipo_dato);`](#sizeoftipo_dato)
  - [**Punteros**](#punteros)
    - [**Aritmética de punteros**](#aritmética-de-punteros)
      - [`Operador de dirección (&):`](#operador-de-dirección-)
      - [`Operador de indirección (*)`](#operador-de-indirección-)
      - [`Incrementos (++) y decrementos (--)`](#incrementos--y-decrementos---)
    - [**void\*: el puntero comodín**](#void-el-puntero-comodín)
    - [**Puntero a un arreglo**](#puntero-a-un-arreglo)
    - [**Puntero a una función**](#puntero-a-una-función)
  - [**Análisis de algoritmos**](#análisis-de-algoritmos)
    - [**Calculo de Tiempo**](#calculo-de-tiempo)
    - [**Big O Notation**](#big-o-notation)
    - [**Teorema maestro**](#teorema-maestro)
      - [**Casos Irresolubles**](#casos-irresolubles)
    - [**Big-Omega Ω:**](#big-omega-ω)
    - [**Big-Theta Θ:**](#big-theta-θ)
    - [**Tabla de complejidad de Estructura de Datos**](#tabla-de-complejidad-de-estructura-de-datos)
    - [**Tabla de complejidad de Algoritmos de Ordenamiento**](#tabla-de-complejidad-de-algoritmos-de-ordenamiento)
  - [**Recursividad:**](#recursividad)
    - [**Recursiva de cola**](#recursiva-de-cola)
    - [**Backtracking**](#backtracking)
  - [**Tipo de dato abstracto (TDA)**](#tipo-de-dato-abstracto-tda)
    - [**Caja Negra y Caja Blanca**](#caja-negra-y-caja-blanca)
    - [**Contrato entre el implementador y el usuario**](#contrato-entre-el-implementador-y-el-usuario)
  - [**Testing**](#testing)
    - [**Pruebas Unitarias - Análisis Dinámico**](#pruebas-unitarias---análisis-dinámico)
    - [**Pruebas de Integración - Análisis Dinámico**](#pruebas-de-integración---análisis-dinámico)
    - [**TDD: Test-Driven Developmnet**](#tdd-test-driven-developmnet)
  - [**Nodo Enlazado (TDA)**](#nodo-enlazado-tda)
    - [**Implementacion:**](#implementacion)
      - [**Simplemente Enlazado:**](#simplemente-enlazado)
      - [**Doblemente Enlazado:**](#doblemente-enlazado)
  - [**TDA Lista**](#tda-lista)
    - [**Conjunto mínimo de operaciones Lista:**](#conjunto-mínimo-de-operaciones-lista)
    - [**Lista enlazada**](#lista-enlazada)
      - [**Lista simplemente enlazada:**](#lista-simplemente-enlazada)
        - [**Lista simplemente enlazada Visulizacion de Pasos:**](#lista-simplemente-enlazada-visulizacion-de-pasos)
      - [**Lista doblemente enlazada:**](#lista-doblemente-enlazada)
        - [**Lista doblemente enlazada Visulizacion de Pasos:**](#lista-doblemente-enlazada-visulizacion-de-pasos)
      - [**Lista circular**](#lista-circular)
  - [**TDA Pila**](#tda-pila)
    - [**Conjunto mínimo de operaciones Pila:**](#conjunto-mínimo-de-operaciones-pila)
  - [**TDA Cola**](#tda-cola)
    - [**Conjunto mínimo de operaciones Cola:**](#conjunto-mínimo-de-operaciones-cola)
  - [**TDA Iterador - Lista**](#tda-iterador---lista)
    - [**Iterador Interno**](#iterador-interno)
    - [**Iterador Externo**](#iterador-externo)
      - [Conjunto mínimo de operaciones Iterador Externo:](#conjunto-mínimo-de-operaciones-iterador-externo)
  - [**TDA Árbol**](#tda-árbol)
    - [**Definiciones:**](#definiciones)
    - [**Tipos de árboles:**](#tipos-de-árboles)
  - [**TDA Árbol Binario**](#tda-árbol-binario)
    - [**Factor de equilibrio:**](#factor-de-equilibrio)
    - [**Conjunto mínimo de operaciones Arbol Binario:**](#conjunto-mínimo-de-operaciones-arbol-binario)
    - [**Recorridos:**](#recorridos)
  - [**TDA Árbol Binario de Búsqueda**](#tda-árbol-binario-de-búsqueda)
    - [**Conjunto mínimo de operaciones ABB:**](#conjunto-mínimo-de-operaciones-abb)
    - [**Representacion de operaciones:**](#representacion-de-operaciones)
  - [**TDA Árbol Adelson-Velsky and Landis (AVL)**](#tda-árbol-adelson-velsky-and-landis-avl)
    - [**Rotaciones:**](#rotaciones)
      - [**Rotación simple a derecha:**](#rotación-simple-a-derecha)
      - [**Rotación simple a izquierda**](#rotación-simple-a-izquierda)
      - [**Rotación doble izquierda - derecha:**](#rotación-doble-izquierda---derecha)
      - [**Rotación doble derecha - izquierda.**](#rotación-doble-derecha---izquierda)
  - [**TDA Árbol Rojo Negro (ARN)**](#tda-árbol-rojo-negro-arn)
    - [**Reglas:**](#reglas)
    - [**Inserción: `O(log(n))`:**](#inserción-ologn)
      - [**Casos al insertar:**](#casos-al-insertar)
    - [**Eliminación: `O(log(n))`:**](#eliminación-ologn)
    - [**Rebalanceo:**](#rebalanceo)
  - [**TDA Árboles B**](#tda-árboles-b)
    - [**Características:**](#características)
    - [**Inserción**](#inserción)
    - [**Eliminación - nodo hoja**](#eliminación---nodo-hoja)
    - [**Eliminación - nodo interno**](#eliminación---nodo-interno)
  - [**TDA - Heap Binario**](#tda---heap-binario)
    - [**Estructura del Heap Binario**](#estructura-del-heap-binario)
    - [**Conjunto mínimo de operaciones Heap Binario:**](#conjunto-mínimo-de-operaciones-heap-binario)
      - [**Sift-down y sift-up `O(logn)`**](#sift-down-y-sift-up-ologn)
      - [**Insertar `O(logn)`**](#insertar-ologn)
      - [**Extraer Raiz `O(logn)`**](#extraer-raiz-ologn)
      - [**Heapify:  `O(n)`**](#heapify--on)
      - [**Demostracion de la complejidad de Heapify:**](#demostracion-de-la-complejidad-de-heapify)
  - [**Métodos de Ordenamiento**](#métodos-de-ordenamiento)
    - [**Tipos de Ordenamientos:**](#tipos-de-ordenamientos)
    - [**Quick Sort**](#quick-sort)
      - [**Pasos Quick Sort:**](#pasos-quick-sort)
      - [**Implementación Quick Sort:**](#implementación-quick-sort)
      - [**Complejidad Quick Sort:**](#complejidad-quick-sort)
    - [**Merge Sort**](#merge-sort)
      - [**Pasos Merge Sort**](#pasos-merge-sort)
      - [**Implementación Merge Sort:**](#implementación-merge-sort)
    - [**Bucket Sort**](#bucket-sort)
      - [**Pasos Bucket Sort**](#pasos-bucket-sort)
      - [**Complejidad Bucket Sort**](#complejidad-bucket-sort)
    - [**Counting Sort**](#counting-sort)
      - [**Pasos Counting Sort:**](#pasos-counting-sort)
    - [**Radix Sort**](#radix-sort)
      - [**Pasos Radix Sort:**](#pasos-radix-sort)
    - [**Heapsort**](#heapsort)
  - [**TDA Tabla de Hash**](#tda-tabla-de-hash)
    - [**Funcion de Hash**](#funcion-de-hash)
    - [**Colisión y sus resoluciones**](#colisión-y-sus-resoluciones)
      - [**Hash Abierto, Direccionamiento cerrado:**](#hash-abierto-direccionamiento-cerrado)
      - [**Direccionamiento abierto**](#direccionamiento-abierto)
        - [**Diferentes técnicas de direccionamiento abierto para resolver una colisión:**](#diferentes-técnicas-de-direccionamiento-abierto-para-resolver-una-colisión)
          - [**Problemas y solucion al borrar en un direccionamiento abierto:**](#problemas-y-solucion-al-borrar-en-un-direccionamiento-abierto)
        - [**Area de desbordamiento**](#area-de-desbordamiento)
      - [**Diferencias entre direccionamientos**](#diferencias-entre-direccionamientos)
    - [**Factor de carga**](#factor-de-carga)
    - [**Rehash**](#rehash)
      - [**¿Cómo se hace el rehashing?**](#cómo-se-hace-el-rehashing)
    - [**Operaciones del Hash**](#operaciones-del-hash)
  - [**Grafos**](#grafos)
    - [**Terminología de Grafos:**](#terminología-de-grafos)
    - [**Representación de Grafos:**](#representación-de-grafos)
      - [**Matriz de adyacencia:**](#matriz-de-adyacencia)
        - [**Ventajas y desventajas de las matrices de adyacencia**](#ventajas-y-desventajas-de-las-matrices-de-adyacencia)
      - [**Lista de adyacencia:**](#lista-de-adyacencia)
        - [**Ventajas y desventajas de las listas de adyacencia**](#ventajas-y-desventajas-de-las-listas-de-adyacencia)
      - [**Matriz de Incidencia:**](#matriz-de-incidencia)
    - [**Recorridos de grafos:**](#recorridos-de-grafos)
      - [**Recorrido en profundidad (DFS):**](#recorrido-en-profundidad-dfs)
      - [**Recorrido en ancho (BFS):**](#recorrido-en-ancho-bfs)
    - [**Orden topológico**](#orden-topológico)
    - [**Algoritmos de camino minimo:**](#algoritmos-de-camino-minimo)
      - [**Dijkstra**](#dijkstra)
      - [**Floyd-Warshall**](#floyd-warshall)
    - [**Algoritmos minimum spanning tree:**](#algoritmos-minimum-spanning-tree)
      - [**Algoritmo de Prim:**](#algoritmo-de-prim)
      - [**Algoritmo de Kruskal:**](#algoritmo-de-kruskal)
  - [♡ Visualizadores ♡](#-visualizadores-)

## **Estructura**

Etapas de un programa:

- Edición: archivo de texto editado en cierto lenguaje -> código fuente.
- Compilación y Enlazado.
- Ejecución.
Compilador: Programa que se encarga de traducir código fuente en algo que la computadora pueda entender. No es sencillo: está compuesto por varias etapas, en la cual intervienen diversos componentes del compilador.

![Estructura](Imagenes/Estructura-1.png)

## **Memoria**

Es parte del hardware de la computadora. Puede ser vista como un conjunto de celdas, en el que cada celda posee un valor identificatorio único: la longitud de este depende de la estructura de la computadora (32 o 64 bits). Los valores son contiguos.

![Memoria](Imagenes/memoria.jpeg)

### **Memoria Dinámica**

Memoria reservada y utilizada sólo en tiempo de ejecución.
Es reservada en la sección “heap”.
Es controlada pura y exclusivamente por el programador. Hacer un mal uso de la misma (no liberar memoria, por ejemplo) implica serios problemas en el comportamiento del programa.

**Teorema de Mendez:**
> “La memoria dinámica no se conserva. Siempre que se crea, debe destruirse”

Al principio y al fin del programa debe haber la misma cantidad de memoria dinámica reservada: 0 bytes. Se utiliza Valgrind para controlar que eso se cumpla.

Para utilizarla, el programador tiene que hacerlo solicitándola explícitamente al sistema operativo. En C se la solicita con [`variable = malloc(tamanio_de_memoria)`](#puntero--malloctamanio_de_memoria). Variable es un puntero que apunta a la dirección de memoria obtenida.

Luego de usarla, hay que liberarla. En C se hace con [`free(variable);`](#freepuntero).

En C se utiliza la librería stdlib.h para acceder a esas funciones.

### **Funciones de memoria en C:**

#### `puntero = malloc(tamanio_de_memoria);`

Reserva bytes y devuelve un puntero a la memoria reservada *(la dirección de memoria)*.

- Devuelve NULL si no se pudo reservar memoria.
- Si pudo reservar la memoria, ésta está vacía.

El parámetro recibido está en bytes, por lo que generalmente se hace uso del [`sizeof(tipo de variable)`](#sizeoftipo_dato).
Si la dirección que nos devuelve se pierde sin antes ser liberada, estamos perdiendo memoria.

#### `puntero = calloc(tamanio_vector, tamanio_de_cada_posicion);`

Reserva memoria en el heap, inicializando como 0 su contenido(NULL o false en el caso de dichos tipos). tamanio_vector sería la cantidad de elementos que queremos que el vector pueda almacenar, tamanio_de_cada_posicion, seria el tamaño ([`sizeof()`](#sizeoftipo_dato)) de un elemento de los que se quieren guardar en el vector.

#### `puntero = realloc(puntero, nuevo_tamanio);`

Modifica el tamaño del bloque de memoria al que apunta “puntero” *(recordar que es memoria contigua)*.
El nuevo bloque de memoria ocupado en la memoria dinámica permanecerá sin cambios desde la primera posición hasta el mínimo entre el nuevo y el viejo tamaño.
Al solicitar memoria, pueden ocurrir 3 cosas:

- Se obtiene la memoria solicitada.
- Al solicitar más memoria de la que se tenía, puede suceder que no se pueda agrandar el bloque de memoria en el lugar:
- Se solicita otro bloque de memoria (con otra dirección).
- Se copia toda la información que se tenía del viejo bloque al nuevo bloque.
- Se libera la memoria que se tenía.
- Falla y devuelve NULL.

Por las dudas hay que igualar el realloc a un puntero auxiliar, y si ese puntero es diferente de NULL, recién igualarlo al puntero que se quería agrandar.

Si se agranda el tamaño de la memoria a la que apunta el puntero, la nueva memoria no está inicializada.

Además, hay excepciones en su uso según los parámetros recibidos.

- Si el puntero no apunta a ninguna dirección de memoria (puntero = NULL), la función actúa como `malloc()`.
- Si “nuevo_tamanio” es 0, la función puede actuar como `free()`.

#### `free(puntero);`

Libera la memoria dinámica reservada en dicha dirección a la que apunta puntero:

1. Primero se libera la memoria reservada.
2. Luego se cambia el valor al que apuntaba el puntero (en clase dijeron que no vuelve a ser NULL, pero en los apuntes dice que sí)

Si la memoria ya fue liberada, habrá un comportamiento indefinido.

Si se libera algo nulo (NULL), es irrelevante -es mala práctica-.

#### `sizeof(tipo_dato);`

Se suele utilizar “sizeof()” para calcular los bytes que ocupa un tipo de dato, así saber la cantidad de memoria necesaria para reservar. Por ejemplo:
`int* puntero = malloc(sizeof(int));` -> nos reserva espacio para un int.
`int* puntero = malloc(sizeof(int)*5);` -> nos reserva espacio para un vector de 5 int.
`int** puntero = malloc(sizeof(int*));` -> nos reserva espacio para un puntero a int.
`int** puntero = malloc(sizeof(int*)*5;` -> nos reserva espacio para un vector de punteros a int.

> Recordar que todos los punteros ocupan lo mismo, es decir:
> `sizeof(char*) == sizeof(int*)`

## **Punteros**

Un puntero no es mas que una variable que almacena una direccion de memoria.
Se las declara con el tipo de dato al que apuntarán.

Múltiples indirecciones: Es un puntero que contiene la dirección de memoria de otro puntero.

### **Aritmética de punteros**

Los punteros tienen definidos operadores, los más utilizados son:

#### `Operador de dirección (&):`

Permite acceder a la dirección de memoria de una variable.

Si hago &puntero, obtengo la dirección de memoria donde se encuentra almacenado del puntero, no la dirección que contiene el puntero (a la que apunta).

#### `Operador de indirección (*)`

Permite ver el valor al que apunta un puntero.
Sirve para declarar un puntero: una variable e indicar que la misma contendrá la dirección de memoria de una variable del tipo de dato declarado.

#### `Incrementos (++) y decrementos (--)`

Sirve para moverse entre las direcciones de memoria, recordando que un puntero a un arreglo apunta a un bloque de memoria contigua.

```c
array = &array[0];
array++ = .&array[1];
*array = array[0] = *(array + 0);
(*array)++ = array[0]++ = *(array + 0)++;
```

### **void\*: el puntero comodín**

- Es un puntero que puede apuntar a cualquier cosa, al contrario de cualquier otro tipo de puntero.
- No puede ser desreferenciado directamente: primero hay que castear.
- No pueden aplicarse los operadores + y -, ya que no posee un tamaño de elemento (justamente porque no posee tipo).
- Es posible que gcc y algún otro compilador permita utilizar los operadores + y - si no se utilizan flags de compilación estrictos. En ese caso, toma como tamaño de elemento 1 byte.

```c
void* puntero_comodin;
int i = 1;
char* puntero_char;

puntero_comodin = &i;
puntero_comodin = puntero_char;
```

Desventajas:

- No puede ser desreferenciado directamente: primero hay que castear.
Por ejemplo:

```c
int a = 10;
void* ptr = &a;
printf ("%d", *(int*)ptr); 
```

-> primero lo casteo a un tipo de dato puntero (int*), y luego lo desreferencio (*).

- No pueden aplicarse los operadores.

### **Puntero a un arreglo**

Al tener un puntero a un arreglo, al hablar del arreglo, nos referimos a la primera posición del mismo: `*array = array[0]`.

Al avanzar de posición de memoria `(array + i)`, habremos avanzado de posición de memoria del puntero, para acceder al valor deberemos hacer `*(array + i)`.

Recordemos entonces que:

```c
array = &array[0];
*array = array[0] = *(array + 0);
```

### **Puntero a una función**

Un puntero a una función contiene la dirección de memoria de una función en un programa.
Diferencias con los punteros normales:

- Apuntan a código y no a datos, apuntando normalmente a la primera instrucción ejecutable de la función.
- Como los punteros normales, no se debe solicitar ni liberar memoria para utilizarlos.

**Declaración:**

```c
tipo_de_retorno (*nombre_del_puntero)(parametro_1, parametro_2, …, parametro_n);
```

Ejemplos de implementación (se puede hacer de muchas formas pero dejo la mejor):

```c
void(*funcion_matematica)(int, int);
funcion_matematica = sumar;
funcion_matematica(1, 2);
```

## **Análisis de algoritmos**

Algoritmo: es una secuencia de pasos:

- no ambiguos
- finitos
- precisos

que al ser ejecutada resuelve un problema.

Una vez analizado un problema, diseñado un algoritmo e implementado la solución, es necesario determinar cuántos recursos computacionales (tiempo y espacio) consume el mismo.

### **Calculo de Tiempo**

- **Instrucciones consecutivas:** es el `max(instruccion_1,instruccion_2`
- **Condicionales:** Es el max de los condicionales
- **Iteraciones:** El tiempo de ejecuccion de es `max(instruccion_dentro)*cantidad_interaciones`
- **Iteraciones Anidadas:** Se realizan de dentro hacia afuera `tiempo_ejecucion_instruccion * cant_iteraciones_por_iterador`

### **Big O Notation**

Es una nota matemática que describe el comportamiento de una función cuando el argumento tiende hacia un valor particular o infinito. **Notacion asintotica acotada por arriba**

Si un tiempo de ejecución es $O(f(n))$, el máximo tiempo de ejecución es $k.f(n)$, con k cte: es decir, **nos da lo máximo que puede tardar en ejecutarse un algoritmo.**

Puede calcularse sobre el código fuente, contando las instrucciones a ejecutar, y multiplicando por el tiempo requerido por cada instrucción.

```c
instruccion_1;
for(int i = 0; i < n; i++)
  instruccion_2;
```

T(n): tiempo de ejecución
T(n) = t1 + t2*n;

| Funcion   | Nombre             |
|:---------:|:------------------:|
|  `c`      | Constante          |
| `logn`    | Logaritmico        |
| `n`       | Lineal             |
| `nlogn`   | Logaritmo Iterador |
| $`n^2`$   | Cuadratica         |
| $`n^3`$   | Cubica             |
| $`2^n`$   | Exponencial        |
| $`!n`$    | Factorial          |

![Big-O](Imagenes/Big-O%20Complexity.png)

### **Teorema maestro**

Proporciona una solución sencilla en términos asintóticos para ecuaciones de recurrencia que ocurren en muchos algoritmos recursivos de tipo divide y vencerás.

No se puede usar en todos los casos: sirve para ecuaciones de recurrencia del siguiente estilo:

$`T(n)=aT(\frac{n}{b})+n^c\ \ \ donde\ a\ge1,b>1`$

- `n` es el tamaño del problema a resolver.
- `a` es el número de subproblemas en la recursión: es la cantidad de llamadas recursivas que realiza el algoritmo.
- `b` es en cuánto se divide el problema en cada llamada recursiva.
- $`\frac{n}{b}`$ el tamaño de cada subproblema. (Aquí se asume que todos los subproblemas tienen el mismo tamaño)
- $`f(n)=n^c`$ es el costo del trabajo hecho fuera de las llamadas recursivas, que incluye el costo de la división del problema y el costo de unir las soluciones de los subproblemas. Es decir, es el costo en tiempo de lo que cuesta dividir y combinar.

**Solucion:**

- $`Si\ \log_b(a)<c\Rightarrow T(n)=O(n^c)`$
- $`Si\ \log_b(a)=c\Rightarrow T(n)=O(n^c\log(n))`$
- $`Si\ \log_b(a)>c\Rightarrow T(n)=O(n^{log_b(a)})`$

Se puede resolver con [Calculadora de Teorema Maestro](https://www.wolframalpha.com/input/?i=T%5Bn%5D%3D%3D2T%5Bn%2F2%5D%2B1)

#### **Casos Irresolubles**

- $`T(n)=2^nT(\frac{n}{2})+n^n`$
  - a no es una constante; el numero de subproblemas debe ser fijo
- $`T(n)=2T(\frac{n}{2})+\frac{n}{logn}`$
- $`T(n)=0.5T(\frac{n}{2})+n`$
  - a < 1 no puede darse el caso que haya menos de un subproblema
- $`T(n)=64T(\frac{n}{8})-n^2logn`$
  - b <= 1 no puede darse tampoco
  - f(n) no puede ser negativo
- $`T(n)=64T(\frac{n}{8})+n(2-cos(n))`$
  - Caso 3 pero hay una violacion de regularidad

### **Big-Omega Ω:**

Nos indica cuánto es lo mínimo que tarda en ejecutarse un algoritmo.
Está acotada por abajo: Big Omega es el límite inferior asintótico del crecimiento del tiempo de ejecución.

### **Big-Theta Θ:**

Si la tasa de crecimiento es Big-Theta:

$`T(N)=\Theta(h(N))\ si\ y\ solo\ si\ T(N)=O(h(N))\ y\ T(N)=\Omega(h(N))`$

Entonces, decir que $`T(N)=\Theta(g(N))`$ implica que existan c1, c2 y n0 tales que $`0\leq c1 ∗g(n)\leq T(n)\leq c2 ∗g(n)`$ para todo $n \geq n_0$. Hay que tener en cuenta que $`\Theta(n)`$ es un conjunto de funciones y vendria a ser el **tiempo de ejecucion medio**.

### **Tabla de complejidad de Estructura de Datos**

|Estructura de Dato        |Acceder         |Busqueda        |Insertar        |Borrar          |
|--------------------------|----------------|----------------|----------------|----------------|
|Vector                    |O(1)            |O(n)            |O(n)            |O(n)            |
|Pila                      |O(n)            |O(n)            |O(1)            |O(1)            |
|Cola                      |O(n)            |O(n)            |O(1)            |O(1)            |
|Lista Simplemente Enlazada|O(n)            |O(n)            |O(1)            |O(1)            |
|Lista Doblemente Enlazada |O(n)            |O(n)            |O(1)            |O(1)            |
|Tabla de Hash             |N/A             |Θ(1) y O(n)     |Θ(1) y O(n)     |Θ(1) y O(n)     |
|ABB                       |Θ(log(n)) y O(n)|Θ(log(n)) y O(n)|Θ(log(n)) y O(n)|Θ(log(n)) y O(n)|
|Arbol-B                   |O(log(n))       |O(log(n))       |O(log(n))       |O(log(n))       |
|Arbol Rojo-Negro          |O(log(n))       |O(log(n))       |O(log(n))       |O(log(n))       |
|AVL Tree                  |O(log(n))       |O(log(n))       |O(log(n))       |O(log(n))       |

### **Tabla de complejidad de Algoritmos de Ordenamiento**

|Complejidad Tiempo ->|Best       | Average   | Worst     |Complejidad Espacio|
|---------------------|-----------|-----------|-----------|-------------------|
|Quicksort            |Ω(n log(n))|Θ(n log(n))|O(n^2)     |O(log(n))          |
|Mergesort            |Ω(n log(n))|Θ(n log(n))|O(n log(n))|O(n)               |
|Heapsort             |Ω(n log(n))|Θ(n log(n))|O(n log(n))|O(1)               |
|Bubble Sort          |Ω(n)       |Θ(n^2)     |O(n^2)     |O(1)               |
|Insertion Sort       |Ω(n)       |Θ(n^2)     |O(n^2)     |O(1)               |
|Selection Sort       |Ω(n^2)     |Θ(n^2)     |O(n^2)     |O(1)               |
|Bucket Sort          |Ω(n+k)     |Θ(n+k)     |O(n^2)     |O(n)               |
|Radix Sort           |Ω(nk)      |Θ(nk)      |O(nk)      |O(n+k)             |
|Counting Sort        |Ω(n+k)     |Θ(n+k)     |O(n+k)     |O(k)               |

## **Recursividad:**

Concepto que hace referencia a contenerse a sí mismo en una versión más pequeña.
**Dos tipos:**

- **Indirecta:** Hay una secuencia de llamados recursivos, donde al menos uno de estos invoca al llamado inicial. Tienen:
  - Una condicion de corte
  - Una llamada recursiva
- **Directa:** Es llamada por sí misma.

Los algoritmos recursivos tienden a utilizar el “divide y conquista”: partir tu problema es problemas similares, pero más chiquitos hasta llegar a la unidad, que es indivisible: lo resolves (porque es más fácil), y después juntas todos los resultados.

```mermaid
graph LR
    A[Llamada a la<br>   funcion] --> B{Condicion<br> de Corte};
    B -->|false| A;
    B -->|true| E[Termino];
```

### **Recursiva de cola**

La llamada recursiva es la última operación dentro de dicha función.
Es decir, no queda almacenado el valor en el stack, porque se sobreescribe la información necesaria para la recursión.

Técnico: es una optimización que hace el compilador cuando lo último que haces en la función es hacer la llamada recursiva, se puede hacer algo que es la optimización de recursividad de cola: no acumular las funciones del stack, solo la llamada.

### **Backtracking**

Es una técnica de resolución de problemas que explora recursivamente todas las soluciones posibles. Ejemplo: [Sudoku](Backtracking/Sudoku/), [Laberinto](Backtracking/Laberinto/).

## **Tipo de dato abstracto (TDA)**

Un Tipo de Dato Abstracto en programación se le considera al modelo que define valores y operaciones que se pueden realizar sobre él.

Lo más importante es determinar qué datos y operaciones son relevantes para mi problema.

Al conjunto de operaciones que deberían estar se le denomina conjunto mínimo de operaciones.
Se crea una biblioteca con una o varias estructuras relacionadas a este tipo de dato.
Se le llama abstracto ya que se busca concentrarse solo en lo que se necesita e importa a la hora de implementarlo.

- **Beneficios:**
  - Al no tener acceso al código, el usuario no puede romperlo, sabemos que la parte interna del mismo estará bien en todo momento, los casos bordes solo pueden ser generados por los parámetros recibidos en cada función.
  - Poder proveer al usuario de mayor flexibilización a la hora de programar: si una parte cambia, las demás no se ven afectadas. Esto nos lo brinda la abstracción: manejan la abstracción.
  - Quien lo utiliza no necesita conocer internamente cómo funciona: necesita saber qué hace, no cómo se hace: encapsulamiento. A este concepto se lo conoce como "caja blanca".
  - Se fuerza la modularización: es más fácil localizar errores y realizar cambios.

Lo ideal es que el usuario reciba el .h y un .o para utilizarlo, sin nada más.

### **Caja Negra y Caja Blanca**

Son conceptos que refieren a cómo se ve una solución:

- Si se ve qué hace, se ve con una caja negra, oscura, que solo deja ver el exterior.
- Si se ve cómo lo hace, se ve con una caja blanca, como transparente, se puede ver el interior.

La visión de aquel que utiliza un TDA es de caja negra.

### **Contrato entre el implementador y el usuario**

Al realizar la abstracción, se genera un contrato entre el implementador y el usuario: El implementador debe garantizar que la funcionalidad expuesta en el archivo cabecera (.h) funciona como se dice.**Hay un que y mil comos**

![Contrato](Imagenes/Contrato-1.png)

## **Testing**

**Definiciones:**

- **Falla:** Defecto no detectado que puede producir errores inesperados en nuestro programa.
Ejemplo: inicializaciones incorrectas, condiciones mal puestas.
- **Error:**
Estado incorrecto de nuestro programa causado por una falla.
Ejemplo: Bucle infinito.
- **Fallo:**
Comportamiento incorrecto de nuestro programa causado por un error.

Evitarlos: con testing. Se puede testear de dos formas: estáticamente y dinámicamente.

- **Análisis estático:** Se analiza el código escrito (código fuente).

- **Análisis dinámico:** Se analiza el código mientras se ejecuta.

### **Pruebas Unitarias - Análisis Dinámico**

Son pruebas automatizadas.
Se encargan de comprobar el funcionamiento de una unidad de nuestro programa: una función, una clase.

**Anatomía:**

- **Inicialización:** creo e inicializo todo lo que necesito para mi prueba.
- **Afirmación:** me permiten conocer si el comportamiento es el esperado.
- **Destrucción:** destruyo todo lo creado para la prueba, así no molestar a mis otras pruebas.

### **Pruebas de Integración - Análisis Dinámico**

Son pruebas automatizadas.
Se encargan de comprobar el funcionamiento de nuestro sistema en conjunto.

### **TDD: Test-Driven Developmnet**

El Desarrollo guiado por pruebas de software es una practica de programacion que consiste en escribir primero las pruebas(*generalmente [unitarias](#pruebas-unitarias---análisis-dinámico)*), escribir el codigo fuente que pase la prueba sastisfactoriamente y por ultimo refactorizar el codigo escrito.

- Refactoriazar: alterar la estructura interna sin cambiar el codigo fuente.

![TDD](Imagenes/TDD.png)

## **Nodo Enlazado (TDA)**

Es un tipo de dato abstracto muy utilizado.

La unidad del nodo es un contenedor de datos que se abstrae a conocer solo al siguiente y/o al anterior.

### **Implementacion:**

#### **Simplemente Enlazado:**

```c
typedef strcut nodo{
    void* elemento;
    nodo_t* sigiuente;
}
```

![Nodo Simplemente Enlazado](Imagenes/nodo_simple.png)

#### **Doblemente Enlazado:**

```c
typedef strcut nodo{
    void* elemento;
    nodo_t* sigiuente;
    nodo_t* anterior;
}
```

![Nodo Doblemente Enlazado](Imagenes/nodo_doble.png)

## **TDA Lista**

Una lista es un tipo de dato abstracto.

**Ventajas:**

- Los elementos no tienen que estar contiguos, pueden estar dispersos, cada uno conteniendo información de la ubicación del siguiente elemento.
Desventajas:
- No podemos acceder a una posición random de la lista sin recorrerla primero.
- Se trabaja con punteros y memoria dinámica, lo cual incrementa la dificultad de programarlas.

### **Conjunto mínimo de operaciones Lista:**

- `Creación` `O(1)`
- `Inserción` `O(n)`
- `Eliminación` `O(n)`
- `Búsqueda` `O(n)`
- `Obtener cantidad de elementos`
- `Está vacía` `O(1)`
- `Destrucción` `O(n)`

### **Lista enlazada**

Una lista enlazada consiste en una secuencia de [nodos](#nodo-enlazado-tda), en los que se guardan campos de datos arbitrarios y una o dos referencias o punteros para ubicar al nodo anterior o siguiente. El gran beneficio es que los [nodos](#nodo-enlazado-tda) no deben estar contiguos: pueden estar totalmente dispersos.

#### **Lista simplemente enlazada:**

![Lista Simplemente Enlazada](Imagenes/lista_simplemente_enlazada.png)

La lista simplemente enlazada se caracteriza por tener un enlace por nodo: nos da información de cómo ubicar al siguiente nodo: puede ser que contenga la dirección del siguiente nodo o NULL.

`No se puede volver al nodo anterior`

##### **Lista simplemente enlazada Visulizacion de Pasos:**

<details> <summary><b>Insercion:</b></summary>

- Al principio: ![Lista Simplemente Enlazada Insercion Inicio](Imagenes/lista_simplemente_enlazada_insercion_inicio.gif)
- En posicion ![Lista Simplemente Enlazada Insercion en Posicion](Imagenes/lista_simplemente_enlazada_insercion_en_posicion.gif)
- Al final: ![Lista Simplemente Enlazada Insercion Final](Imagenes/lista_simplemente_enlazada_insercion_final.gif)
</details></br>

<details> <summary><b>Borrado:</b></summary>

- Al principio: ![Lista Simplemente Enlazada Borrado Inicio](Imagenes/lista_simplemente_enlazada_borrado_inicio.gif)
- En posicion: ![Lista Simplemente Enlazada Borrado en Posicion](Imagenes/lista_simplemente_enlazada_borrado_en_posicion.gif)
- Al Final: ![Lista Simplemente Enlazada Borrado Final](Imagenes/lista_simplemente_enlazada_borrado_fin.gif)

</details></br>

#### **Lista doblemente enlazada:**

![Lista Doblemente Enlazada](Imagenes/lista_doblemente_enlazada.png)

La lista doblemente enlazada, por lo contrario, se caracteriza por tener dos enlaces por nodo: nos habla sobre la ubicación del nodo anterior y del siguiente. Resuelve el inconveniente del caso anterior, al poder acceder a nodos anteriores al actual. Entonces, se puede recorrer la lista desde el final hasta el inicio, y al revés.

##### **Lista doblemente enlazada Visulizacion de Pasos:**

<details> <summary><b>Insercion:</b></summary>

- Al principio: ![Lista Doblemente enlazada Insercion Principio](Imagenes/lista_doblemente_enlazada_insercion_inicio.gif)
- En Posicion: ![Lista Doblemente enlazada Insercion Principio](Imagenes/lista_doblemente_enlazada_insercion_en_posicion.gif)
- Al final: ![Lista Doblemente enlazada Insercion Principio](Imagenes/lista_doblemente_enlazada_insercion_final.gif)

</details></br>

<details> <summary><b>Borrado:</b></summary>

- Al Principio: ![Lista Doblemente enlazada Borrado Principio](Imagenes/lista_doblemente_enlazada_borrado_inicio.gif)
- Al final: ![Lista Doblemente enlazada Borrado Final](Imagenes/lista_doblemente_enlazada_borrado_final.gif)
- En posicion: ![Lista Doblemente enlazada Borrado en Posicion](Imagenes/lista_doblemente_enlazada_borrado_en_posicion.gif)

</details></br>

#### **Lista circular**

La lista circular…

## **TDA Pila**

- Una pila es un tipo de dato abstracto.
- LIFO: Last In First Out. Las inserciones y extracciones se hacen siempre desde la última posición.
  - Es muy parecida a una pila de objetos: Para entenderlo, siempre se hace analogía a una pila de platos: el último que colocas es el primero que agarras.

- En todo momento, a diferencia de la lista, solo se tiene acceso al último valor de la estructura.
- Implementaciones:
  - Con nodos
  - Con un vector estatico
  - COn un vector dinamico

**Ventajas:**

Los elementos no tienen que estar contiguos, pueden estar dispersos, cada uno conteniendo información de la ubicación del siguiente elemento.

### **Conjunto mínimo de operaciones Pila:**

- `Creación` `O(1)`
- `Apilar o Push` `O(1)`
- `Desapilar o Pop` `O(1)`
- `Obtener tope` `O(1)`
- `Está vacía` `O(1)`
- `Destrucción` `O(n)`

![TDA Pila](Imagenes/TDA_Pila.gif)

## **TDA Cola**

- Una cola es un tipo de dato abstracto.
- **FIFO**(First In First Out): Se inserta por el final pero se elimina por la primera posición.
  - Una analogía es la cola del supermercado: se atiende al primero de todos y si te queres sumar a la cola, tenés que ponerte al final de la misma.

- Solo se tiene información del primer elemento de la estructura.
- Implementaciones:
  - Con nodos
  - Con un vector estatico
  - COn un vector dinamico

**Ventajas:**

Los elementos no tienen que estar contiguos, pueden estar dispersos, cada uno conteniendo información de la ubicación del siguiente elemento.

### **Conjunto mínimo de operaciones Cola:**

- `Creación` `O(1)`
- `Encolar o Enqueue`
- `Desecolar o Dequeue`
- `Obtener primero` `O(1)`
- `Está vacía` `O(1)`
- `Destrucción` `O(n)`

![TDA Cola](Imagenes/TDA_Cola.gif)

## **TDA Iterador - Lista**

Un iterador es un tipo de puntero que es utilizado por un algoritmo para recorrer los elementos almacenados en un contenedor, sin tener en cuenta su implementación específica.

Un iterador tiene dos operaciones principales: referenciar un elemento particular en una colección, y modificarse a sí mismo para que apunte al siguiente elemento.

El objetivo principal de un iterador, es permitir al usuario procesar cada elemento de una colección sin que el usuario sepa sobre la estructura interna de la misma. Esto permite a la colección almacenar elementos en cualquier forma, mientras permite al usuario tratarlo como si fuera una simple secuencia o lista de elementos.

**Hay dos tipos de iteradores:**

- Iterador interno.
- Iterador externo (podría ser un TDA aparte).

Normalmente el iterador que maneja el usuario es llamado "iterador externo", en cambio, si el iterador auto maneja la iteración, se conoce como "iterador interno".
Ambos iteradores deberían ser igual de eficientes.

### **Iterador Interno**

El iterador interno es el que nos permite recorrer los elementos de un TDA sin conocer la estructura interna del mismo. Se suele hacer esto usando una función la cual se envía como parámetro al llamar a este iterador interno. Además, se le suele enviar un contexto, y así controlar u obtener datos del recorrido del TDA.

### **Iterador Externo**

Es un TDA: nos provee un grupo de primitivas especiales para recorrer una lista. La idea es que conozca cómo está representada la lista a recorrer.

Este iterador tiene un puntero apuntando a una posición de la lista. Cada vez que se quiera acceder al siguiente elemento, simplemente se avanza el puntero una posición.

#### Conjunto mínimo de operaciones Iterador Externo:

- `Creación`
- `Avanzar`
- `Tiene siguiente`
- `Elemento actual`
- `Destruir`

## **TDA Árbol**

Un árbol es un tipo de dato abstracto, cuyo objetivo es poder generar un acceso a los datos más eficiente que `O(n)`.
**Su naturaleza es altamente recursiva**.

Un árbol es una colección de nodos. Los nodos son los elementos o vértices del árbol.
Esta colección puede estar vacía, de otra forma, un árbol consiste en un nodo que se distingue del resto, llamado **raíz**, y cero o muchos subárboles no vacíos, cada uno con su raíz conectada mediante un vértice al nodo raíz del árbol principal.

### **Definiciones:**

- **Raíz:** El nodo superior de un árbol.
- **Hijo:** Un nodo conectado directamente con otro cuando se aleja de la raíz.
- **Padre:** La noción inversa de hijo.
- **Hermanos:** Un conjunto de nodos con el mismo padre.
- **Descendiente:** Un nodo accesible por descenso repetido de padre a hijo.
- **Ancestro:** Un nodo accesible por ascenso repetido de hijo a padre.
- **Hoja:** Un nodo sin hijos.
- **Camino:** Es cómo acceder desde la raíz del árbol principal a cualquier nodo del mismo. Siempre es único. De un nodo a sí mismo la longitud del camino es 0.
- **Profundidad:** Es la mayor cantidad de nodos entre un nodo hoja y el nodo raíz. La altura de un nodo hoja es 0.

**Ventajas:**

A diferencia de los otros TDAs, se puede acceder más rápido a los datos: en promedio, $`O(log(n))`$. Justamente, de esta forma las búsquedas ya no son lineales y se genera una menor complejidad del código.

### **Tipos de árboles:**

- Árbol N-arios o Generales.
- [Árbol Binario](#tda-árbol-binario).
- [Árbol Binario de Búsqueda](#tda-árbol-binario-de-búsqueda).
- [Árbol AVL](#tda-árbol-adelson-velsky-and-landis-avl).
- Árbol Splay.
- [Árbol B](#tda-árboles-b).
- Árbol B+.
- Árbol B*.
- Árbol Rojo Negro.

## **TDA Árbol Binario**

Un árbol binario (AB) es un tipo de dato abstracto, cuya idea es la siguiente: dentro de la misma, hay nodos, cada uno con la siguiente información: su nodo izquierdo y su nodo derecho. Solo está relacionado con dos nodos (por eso "binario").
Cada nodo del árbol, además es la raíz de un árbol binario.
El árbol binario puede estar o no equilibrado.

**Desventajas:**

- Si no está equilibrado, en el peor de los casos, actúa como una lista enlazada.
- No está ordenado: no podemos buscar información fácilmente.
- Se trabaja con punteros y memoria dinámica, lo cual incrementa la dificultad de programarlos.

### **Factor de equilibrio:**

$`(hd - hi)`$ ó $`(hi - hd)`$

- Siendo `hd` la altura del sub-arbol y `hi` la altura del sub-arbol izquierdo
- Siempre tengo que usar la misma convención.

La altura del subárbol derecho y del subárbol izquierdo nunca debe diferir en más de una unidad si es que está equilibrado.

![Arbol Balaceado y Desbalanceado](Imagenes/Arbol%20balanceado%20y%20des.png)

### **Conjunto mínimo de operaciones Arbol Binario:**

- `Creación`
- `Inserción`
- `Eliminación`
- `Búsqueda`
- `Recorrido`
- `Está vacío`
- `Destrucción`

### **Recorridos:**

- <details><summary> Inorden </summary>
  
  **Pasos:**
  1. Se visita el nodo izquierdo.
  2. Se visita el nodo actual.
  3. Se visita el nodo derecho.
  
  ![Recorrido Inorden](Imagenes/Recorrido%20Inorden.gif)

  Implementacion:

  ```c
  void recorrido_inorden(nodo_t* raiz,){
    if(!raiz) return;
    recorrido_inorden(raiz->izquierda);
    //Hacer algo con el nodo
    recorrido_inorden(raiz->derecha);
  }
  ```

  </details>

- <details><summary> Preorden </summary>

  **Pasos:**
  1. Se visita el nodo actual.
  2. Se visita el nodo izquierdo.
  3. Se visita el nodo derecho
  
  ![Recorrido Preorden](Imagenes/Recorrido%20Preorden.gif)

  Implementacion:

  ```c
  void recorrido_preorden(nodo_t* raiz,){
    if(!raiz) return;
    //Hacer algo con el nodo
    recorrido_preorden(raiz->izquierda);
    recorrido_preorden(raiz->derecha);
  }
  ```

  </details>

- <details><summary> Postorden </summary>
  
  **Pasos:**
  1. Se visita el nodo izquierdo.
  2. Se visita el nodo derecho.
  3. Se visita el nodo actual.
  
  ![Recorrido Postorden](Imagenes/Recorrido%20Postorden.gif)
  
  Implementacion:

  ```c
  void recorrido_postorden(nodo_t* raiz,){
    if(!raiz) return;
    recorrido_postorden(raiz->izquierda);
    recorrido_postorden(raiz->derecha);
    //Hacer algo con el nodo
  }
  ```

  </details>

- <details><summary> En anchura </summary>

  ![Recorrido en Anchura](Imagenes/Recorridio%20en%20anchura.gif)

  Se hace con una cola auxiliar.
   1. Se encola el nodo raíz.
   2. Se desencola un elemento, se encola a sus hijos (derecho e izquierdo).
   3. Se repite el segundo paso hasta que no haya más elementos en la cola.

    Cuando en la cola hay un elemento: primer nivel.

    Cuando en la cola hay por primera vez dos elementos o menos: segundo nivel.

    Cuando en la cola hay por primera vez cuatro elementos o menos: tercer nivel…

    ```c
      #include "cola.h"

      void recorrido_en_anchura(arbol_t arbol){
        if(!arbol) return;

        cola_t cola = crear();
        nodo_t corriente;

        encolar(cola,arbol);

        while(!vacia(cola)){
          corriente = desencolar(cola);
          //hacer algo

          if(corriente->izquierda != NULL){
            encolar(cola,corriente->izquierda);
          }
          if(corriente->derecha != NULL){
            encolar(cola,corriente->derecha);
          }
        }
        destruir(cola);
      }
    ```
  
</details>

## **TDA Árbol Binario de Búsqueda**

Un árbol binario de búsqueda (ABB) es un tipo de Árbol Binario (AB).

Está ordenado de cierta manera, al contrario de un simple árbol binario. La convención es insertar nodos en el árbol teniendo el siguiente criterio:

- los elementos mayores, a la derecha.
- los elementos menores, a la izquierda.
- si no hay elementos, es el primer elemento del árbol (la raíz).

![ABB](Imagenes/ABB.png)

**Ventajas:**

- Facilidad para buscar (sabemos cómo está ordenado).
- Rapidez al buscar, pudiendo obtener información de algún elemento con una complejidad de o(log(n)), cuando en una lista la complejidad de búsqueda es de o(n).

![ABB Busqueda](Imagenes/ABB%20busqueda.png)

Es factible que un Árbol Binario de Búsqueda se degenere en una lista enlazada: siempre insertando un valor menor al anterior o siempre mayor al anterior.

### **Conjunto mínimo de operaciones ABB:**

- `Creación:` `O(1)`
- `Inserción:` $`O(\log(n))`$ si esta balanceado ó $`O(n)`$ si tiende a Lista
- `Eliminación:` $`O(\log(n))`$ si esta balanceado ó $O(n)`$ si tiende a Lista
- `Búsqueda:` $`O(\log(n))`$ si esta balanceado ó $`O(n)`$ si tiende a Lista
- `Recorrido:` `O(n)`
- `Está vacío:` `O(1)`
- `Destrucción:` `O(n)`

Cuando se crea el árbol binario de búsqueda se hace junto a un comparador o un destructor. Sí o sí tiene que tener un comparador (para saber cómo comparar entre elementos), pero no necesariamente un destructor.

### **Representacion de operaciones:**

**Insercion:**

![ABB Insercion](Imagenes/ABB%20Insercion.png)

**Eliminación:**
  
![ABB Eliminar 1](Imagenes/ABB%20Eliminar%201.png)

![ABB Eliminar 2](Imagenes/ABB%20Eliminar%202.png)

![ABB Eliminar 3](Imagenes/ABB%20Eliminar%203.png)

## **TDA Árbol Adelson-Velsky and Landis (AVL)**

Un AVL es un tipo de árbol binario de búsqueda que se autobalancea, sabiendo que su [factor de equilibrio](#factor-de-equilibrio) es un número entero tal que $`|Fe|\le1`$.

- 0 → el nodo está equilibrado y sus subárboles tienen exactamente la misma altura.
- 1 → el nodo está equilibrado y su subárbol derecho es un nivel más alto.
- -1 → el nodo está equilibrado y su subárbol izquierdo es un nivel más alto.
  
### **Rotaciones:**

Las rotaciones reorganizan la estructura del árbol después de cada inserción o borrado. Para ello se deben recalcular los factores de equilibrio de cada nodo, un costo asociado a tener en cuenta.

![Rotaciones AVL](Imagenes/AVL_Tree_Example.gif)

Existen dos tipos de rotaciones:

- Rotaciones simples.
- Rotaciones dobles.

Antes de realizar cualquier rotación, y después de realizar un cambio en el árbol (eliminación o inserción) debo recalcular los factores de balanceo de la rama modificada.
En el momento que encuentre un factor de balanceo fuera de lo válido ,*|Fe| > 1*, debo realizar la rotación.

#### **Rotación simple a derecha:**

Se hace cuando el subárbol izquierdo es 2 veces mayor al subárbol derecho.

![AVL Rotacion Derecha](Imagenes/AVL%20Rotacion%20Derecha.png)

Todo lo que tenga “b” a su derecha, va a pasar a estar a la izquierda de “c”

<details><summary><b>Implementacion:</b></summary>

```c
struct Node *rightRotate(struct Node *y){
  struct Node *x = y->left;
  struct Node *T2 = x->right;

  //Performe rotation
  x->right = y;
  y->left= T2;

  //Update height
  y->height = max(height(y->left),height(y->right)+1);
  x->height = max(height(x->left),height(x->right)+1);

  return x;
}
```

</details>
</br>

#### **Rotación simple a izquierda**

Se hace cuando el subárbol derecho es 2 veces mayor al subárbol izquierdo.

![AVL Rotacion Izquierda](Imagenes/AVL%20Rotacion%20Izquierda.png)

<details><summary><b>Implementacion:</b></summary>

```c
struct Node *rightRotate(struct Node *x){
  struct Node *y = x->right;
  struct Node *T2 = y->left;

  //Performe rotation
  y->left= x;
  x->right = T2;

  //Update height
  y->height = max(height(y->left),height(y->right)+1);
  x->height = max(height(x->left),height(x->right)+1);

  return y;
}
```

</details>
</br>

#### **Rotación doble izquierda - derecha:**

Se hace cuando el subárbol izquierdo es 2 veces mayor al subárbol derecho y el subárbol izquierdo pesa a derecha.

![AVL Rotacion I-D](Imagenes/AVL%20Rotacion%20I-D.png)

#### **Rotación doble derecha - izquierda.**

Se hace cuando el subárbol derecho es 2 veces mayor al subárbol izquierdo y el subárbol derecho pesa a izquierda.

![AVL Rotacion D-I](Imagenes/AVL%20Rotacion%20D-I.png)

## **TDA Árbol Rojo Negro (ARN)**

Un árbol rojo negro es un árbol binario de búsqueda autobalanceado con características especiales.

- Sus nodos son rojos o negros.

![Arbol R-N](Imagenes/Arbol%20R-N.png)

### **Reglas:**

1. Cada nodo es o rojo o negro.
2. Su raíz siempre es negra.
3. Todas las hojas son negras.
4. Un nodo rojo debe tener hijos negros.
5. Para cada nodo, todos los caminos desde el nodo a hojas inferiores contienen exactamente la misma cantidad de nodos negros.

**Ejemplos de implementaciones erróneas:**

![Arbol R-N Mal](Imagenes/Arbol%20R-N%20mal.png)

Estas características aseguran que el árbol se autobalancee: este autobalanceo no es perfecto pero es lo suficiente eficiente para garantizar una búsqueda en tiempo $O(log(n))$, mismo tiempo para inserciones y eliminaciones.

- El número de nodos negros desde el nodo raíz se denomina profundidad negra del árbol.
- El número uniforme de nodos negros en todos los caminos desde la raíz hasta las hojas se denomina altura-negra.
  
Las dos anteriores hacen que: el camino más largo desde la raíz hasta una hoja no es más largo que dos veces el camino más corto desde la raíz a una hoja: El resultado es que dicho árbol está aproximadamente equilibrado.

Un árbol rojo-negro no n nodo internos tiene como altura como mucho $`2log(n + 1)`$.

**Implementacion en C:**

```c
typedef struct nodo{
  int color;
  int clave
  struct nodo *izq;*der,*padre;
}nodo_t;
```

### **Inserción: `O(log(n))`:**

![Arbol R-N Insercion](Imagenes/Arbol%20R-N%20Insercion.png)

1. Se busca punto a insertar.
2. Se inserta el nodo coloreado de rojo (es la mejor manera de evitar desestructurar el árbol -regla 5.-).
3. Se reestructura el árbol (siempre):
   1. Recoloración.
   2. Rotación.

#### **Casos al insertar:**

- **Caso 1:** El árbol estaba vacío: inserto nodo rojo, rebalanceo a color negro (por ser raíz/hoja).

   ![Arbol R-N Inserta Caso 1](Imagenes/Arbol%20R-N%20Insercion%201.png)

- **Caso 2:** El nodo padre del insertado es negro: inserto nodo rojo.

    ![Arbol R-N Inserta Caso 2](Imagenes/Arbol%20R-N%20Insercion%202.png)

- **Caso 3:** El nodo padre y tío del insertado son rojos.

    ![Arbol R-N Inserta Caso 3](Imagenes/Arbol%20R-N%20Insercion%203.png)

- **Caso 4:** Padre rojo, tío negro: rotación simple a un lado, rotación simple al otro.  

   ![Arbol R-N Inserta Caso 4](Imagenes/Arbol%20R-N%20Insercion%204.png)

**Rotación a izquierda:**

![Arbol R-N Rotacion Derecha](Imagenes/Arbol%20R-N%20Rotacion%20Izquierda.png)

**Rotación a derecha:**

![Arbol R-N Rotacion Derecha](Imagenes/Arbol%20R-N%20Rotacion%20Derecha.png)

### **Eliminación: `O(log(n))`:**

![Arbol R-N Eliminar](Imagenes/Arbol%20R-N%20Eliminar.png)

### **Rebalanceo:**

**Paso 1:**

![Arbol R-N Paso 1](Imagenes/Arbol%20R-N%20Rebalanceo%201.png)

Si el nodo es raíz o rojo: lo pinto de negro.
Sino, voy al Paso 2.

**Paso 2:**

![Arbol R-N Paso 2](Imagenes/Arbol%20R-N%20Rebalanceo%202.png)

Si el hermano es rojo:

- Pinto al hermano de negro.
- Pinto al padre de rojo.
- Hago una rotación: si era hijo derecho, a derecha, sino a izquierda.
Voy al Paso 3.

**Paso 3:**

![Arbol R-N Paso 3](Imagenes/Arbol%20R-N%20Rebalanceo%203.png)

Si el hermano tiene dos hijos negros:

- Pinto al hermano de rojo.
- Ahora mi nodo actual es el padre, voy al Paso 1.
Sino, voy al Paso 4.

**Paso 4:**

![Arbol R-N Paso 4](Imagenes/Arbol%20R-N%20Rebalanceo%204.png)

Si el nodo es hijo izquierdo, y el hijo derecho del hermano es negro:

- Pinto al hermano de rojo.
- Hago una rotación a derecha del hermano.
Si el nodo es hijo derecho, y el hijo izquierdo del hermano es negro:
- Pinto al hermano de rojo.
- Hago una rotación a izquierda del hermano.
Voy al Paso 5.

**Paso 5:**

![Arbol R-N Paso 5](Imagenes/Arbol%20R-N%20Rebalanceo%205.png)

- Pinto al hermano del color del padre.
- Pinto al padre de negro.
Si el nodo es hijo izquierdo:
- Pinto al hijo derecho del hermano de negro.
- Hago una rotación a izquierda del padre.
Si el nodo es hijo derecho:
- Pinto al hijo izquierdo del hermano de negro.
- Hago una rotación a derecha del padre.
Fin.

## **TDA Árboles B**

Los árboles B de búsqueda surgen por la necesidad de tener muchos elementos, tantos que no entran en memoria, como podría ser la población de un país, o los clientes de un banco.

Son árboles de **orden M**, equilibrados, de **Búsqueda y Mínima Altura**.

**Páginas:** nodos del árbol.

**Ramas:** punteros del nodo.

**Por nodo:**

- Mínimo:
  - Punteros: (M+1)/2 (redondeado).
  - Elementos: (M-1)/2 (redondeado).
- Máximo:
  - Punteros: M.
  - Elementos: M-1.

**Raíz:** mínimo 2 punteros, máximo M.

### **Características:**

- El número de claves en cada nodo es siempre una unidad menor que el número de sus ramas.
- Los elementos de un nodo deben estar contiguos: no puede haber espacios libres entre ellos.
- No se habla de nuevos nodos hasta que el actual esté lleno.
- Todas las ramas que parten de un determinado nodo tienen exactamente la misma altura: si un nodo tiene un puntero para la derecha, debe tener otro para la izquierda.
- En los nodos las claves se encuentran clasificadas y, a su vez, clasifican las claves almacenadas en los nodos descendientes.

![Arbol B](Imagenes/Arbol%20B.png)

**Cada nodo posee:**

- Registros.
- Punteros a otros nodos.
- Un contador de registros utilizados.

### **Inserción**

- Si un registro tiene espacio, se inserta como si fuera una ABB.
- Si debería insertar en un registro lleno:
  - Divido los elementos en dos grupos.
  - Dejo en el nodo, como único y primer valor, el valor que esté en el medio (se redondea para arriba gralmente), haciendo que este apunte hacia dos subnodos, cada uno con uno de los grupos de elementos iniciales.
  - En caso de que el nodo padre esté lleno, repito los pasos.
  - En caso de que queden nodos a diferentes alturas, y de ser posible, elevar todo un nodo (dejar el valor del medio en su nodo padre, y de ahí generar dos subnodos).
  - En caso que la raíz desborde, con todos sus punteros diferentes de NULL, hay que crear una nueva raíz.

**Ejemplo inserción**
Estado inicial:

![Arbol B Insertar 1](Imagenes/Arbol%20B%20Insercion%201.png)

Inserto el 40:

![Arbol B Insertar 2](Imagenes/Arbol%20B%20Insercion%202.png)

Ejemplo inserción, insertar en padre: Si quiero insertar un “13”, tengo que:
Si quiero proceder como antes:

![Arbol B Insertar 3](Imagenes/Arbol%20B%20Insercion%203.png)
  
Pero como me quedan nodos a diferente altura, tengo que trabajar con el padre del nodo.
Estado final:

![Arbol B Insertar 4](Imagenes/Arbol%20B%20Insercion%204.png)

Ejemplo nueva raíz: Si quiero insertar el “17”:

![Arbol B Insertar 5](Imagenes/Arbol%20B%20Insercion%205.png)

### **Eliminación - nodo hoja**

- Localizar el nodo a eliminar.
- Si el nodo contiene un número de claves mayor al mínimo, se elimina y listo.
- Sino, completar el nodo con su mínimo de elementos, con alguno de un hermano inmediato:
  - Si el hermano **nodo izquierda** tiene más elementos que el mínimo, muevo su elemento más grande al **nodo padre** y del **nodo padre** bajo el elemento correspondiente hacia el **nodo actual** donde el elemento fue eliminado.
  - Sino, si el hermano **nodo derecha** tiene más elementos que el mínimo, muevo su elemento más chico al **nodo padre** y del **nodo padre** bajo el elemento correspondiente hacia el **nodo actual**, donde el elemento fue eliminado.
  - Sino, creo un nuevo nodo combinando el **nodo actual**, un vecino inmediato y el elemento correspondiente del **nodo padre** (asegurando que el número de elementos no sea mayor a M). Si bajando el elemento correspondiente del **nodo padre**, este queda con menos elementos que el mínimo, entonces propago el proceso hacia arriba, así se reduce la altura del árbol.


### **Eliminación - nodo interno**

- Localizar el nodo a eliminar.
- Intercambiarlo por el predecesor inorden (no hoja).
- Eliminar el predecesor inorden: Actuar frente a la eliminación de un nodo hoja.

## **TDA - Heap Binario**

El Heap binario es un árbol binario que cumple las siguentes propiedades:

- **Es un arbol completo:** que se va llenando de izquierda a derecha. Por lo tanto el único nivel con espacios “vacíos” va a ser el último.
  - Importante: si un nodo heap no tiene hijo a la izquierda, tampoco va a tener hijo a la derecha (por la propiedad de que se llena de izquierda a derecha).
- **Propeidad de Heap:**
  - **Heap Minimal:** Cada nodo es menor o igual a sus hijos. ![Heap Minimal](Imagenes/Heap%20minimal.jpg)
  - **Heap Maximal:** Cada nodo es mayor o igual a sus hijos. ![Heap Maximal](Imagenes/Heap%20maximal.jpg)
- Es de búsqueda y mínima altura.

### **Estructura del Heap Binario**

![Estructura del Heap](Imagenes/Heap_estructura.png)

El heap binario se suele implementar con un vector `v` donde `v[0]` es la raiz y para cada nodo de posicion `i` se puede encontrar el:

- Nodo padre: `v[(i-1)/2]` *excepto para la raiz*
- Nodo hijo izquierdo: `v[(2*i)+1]`
- Nodo hijo derecho: `v[(2*i)+2]`

**Implementacion:**

```c
typedef struct heap{
  void** elementos;
  int tope;
}heap_t;
```

### **Conjunto mínimo de operaciones Heap Binario:**

- `Creación` `O(1)`
- `Heapify`  `O(n)`
- `Inserción` `O(logn)`
- `Extraer Raiz` `O(logn)`
- `Obtener raiz`   `O(1)`
- `Búsqueda` `O(n)`
- `Está vacío` `O(1)`
- `Destrucción` `O(n)`

#### **Sift-down y sift-up `O(logn)`**

- A sift-down tambien se le dice heapify o Heapdown
- A sift-up tambien se le dice  Heapup

<details><summary><b>Implementacion para un heap minimal:</b></summary>

```c
/*
 *
*/
void swap(void** vector,size_t i,size_t j){
    void* aux = vector[i];
    vector[i] = vector[j];
    vector[j] = aux;
}
/*
 * 
*/
void sift_up(heap_t* heap,size_t pos){
    if(heap_vacio(heap) || pos==0){
        return;
    }

    size_t pos_padre = posicion_padre(pos);

    if(heap->vector[pos] < heap->vector[pos_padre]){
        swap(heap->vector,pos,pos_padre);
        sift_up(heap,pos_padre);
    }
}

/*
 *
*/
void sift_down(heap_t* heap,size_t pos){
    if(heap_vacio(heap)){
        return;
    }

    size_t pos_izquierdo = posicion_hijo_izquierdo(pos);
    if(pos_izquierdo >= heap->tope){
        return;
    }
    size_t pos_menor = pos_izquierdo;
    size_t pos_derecha = posicion_hijo_derecho(pos);

    if(pos_derecha < heap->tope){
        if(heap->vector[pos_derecha] < heap->vector[pos_menor]){
            pos_menor = pos_derecha;
        }
    }

    if(heap->vector[pos] > heap->vector[pos_menor]){
        swap(heap->vector,pos,pos_menor);
        sift_down(heap,pos_menor);
    }
}
```

</details>
</br>

#### **Insertar `O(logn)`**

- Se tiene que insertar en el primer lugar sin elemento a la izquierda
  - Para ser un arbol binario completo.
- Se swapean los elementos si:
  - Heap Minimal: es menor al padre
  - Heap maximal: si es mayor al padre

![Heap](Imagenes/Heap%20insercion.gif)

```mermaid
graph LR
    A[Insertar el elemento] --> B[Sift up];
    B -->T[Se compara con el Padre]
    T -->C{Se tiene que swapear?};
    C -->|Si|D[Se cambian el elemento con el<br>del padre];
    C -->|No|E[El heap es correcto];
    D -->B
```

<details><summary><b>Implementacion en c:</b></summary>

```c
/*
 * Recive un heap valido que pueda destruir el elemento
 * Se inserta un elemento en el heap en la posicion correcta
 * Devuelve 0 en caso de exito o -1 en caso de error
*/
int heap_insertar(heap_t* heap,void* elemento){
    if(!heap){
        return ERROR;
    }

    void* aux = realloc(heap->vector,sizeof(void*) * (heap->tope+1));
    if(!aux){
        return ERROR;
    }

    heap->vector = aux;
    heap->vector[heap->tope] = elemento;
    sift_up(heap,heap->tope);
    heap->tope++;
    return EXITO;
}
```

</details>
</br>

#### **Extraer Raiz `O(logn)`**

La operación de eliminación estándar en Heap Binario es eliminar el elemento en el nodo raíz. Es decir, si es un Heap Maximal, la operación de eliminación eliminará el elemento máximo y si es un Heap Minimal, eliminará el elemento mínimo.

1. Reemplazar la raíz o el elemento que se eliminará por el último elemento del arreglo.
2. Eliminar el último elemento del Heap.
3. Puede que luego de esta operación, el árbol no siga cumpliendo con la propiedad del heap, por lo tanto hay que hacer sift down a la nueva raiz.

![Heap extraer](Imagenes/Heap%20borrado.gif)

```mermaid
graph LR
    A[Se Extrae la raiz] --> B[Se cambia con<br>el ultimo elemento del heap<br>por la raiz];
    B -->C[Sift Down];
    C -->Z[Se compara con el hijo izquierdo]
    Z -->D{Se tiene que swapear?};
    D -->|No|F[Se compara el elemnto con el<br>hijo derecho];
    G -->|No|H[El heap es correcto];
    D -->|Si|E[Sawp con el hijo];
    G -->|Si|E;
    E -->C;
    F -->G{Se tiene que swapear?};

```
<details><summary><b>Implementacion en c:</b></summary>

```c
/*
 * Se saca del heap la raiz y se devuelve su el elemento 
*/
void* heap_extraer_raiz(heap_t* heap){
    if(heap_vacio(heap)){
        return NULL;
    }

    void* elemento = heap->vector[0];
    swap(heap->vector,0,heap->tope-1);

    void* aux = realloc(heap->vector,sizeof(void*) * (heap->tope-1));
    /*if(!aux){
        return elemento; // Chequear despues
    }*/
    heap->vector = aux;

    heap->tope--;
    if(heap->tope > 0){
        sift_down(heap,0);
    }

    return elemento;
}
```

</details>
</br>

#### **Heapify:  `O(n)`**

![Heapify](Imagenes/Heapify.gif)

Heapify es el proceso de acomodar un vector como heap, apartir de el se puede crear un Heap desde un vector en `O(n)`. El Heap puede ser Heap Maximal o Heap Minimal.

Supongamos que necesitamos construir un Heap Maximal a partir de los elementos del arreglo de arriba. Puede verse claramente que el árbol binario completo formado en la imagen debajo no sigue la propiedad del heap.

**Pasos:**

1. Comenzar desde el primer índice del nodo no hoja cuyo índice está dado por $`i = \frac{n}{2} - 1`$. Establecer el elemento actual `i` como el más grande.
2. Si el hijo izquierdo es mayor que el elemento actual, establecer el índice del hijo izquierdo como el más grande.
3. Si el hijo derecho es mayor que el elemento más grande, establecer el índice del hijo derecho como el más grande.
   - Si se quisiera construir un Heap Minimal, tanto el hijo izquierdo como el hijo derecho deben ser más pequeños que el padre para todos los nodos.
4. Luego intercambiar el mas grande con el elemento actual `i`.
5. Repetir hasta que el árbol cumpla con la propiedad del heap, es decir, que sea Minimal o Maximal.

<details><summary><b>Implementacion en c:</b></summary>

```c
// To heapify a subtree rooted with node i which is 
// an index in arr[]. N is size of heap 
void heapify(int arr[], int n, int i) 
{ 
    int largest = i; // Initialize largest as root 
    int l = 2 * i + 1; // left = 2*i + 1 
    int r = 2 * i + 2; // right = 2*i + 2 
  
    // If left child is larger than root 
    if (l < n && arr[l] > arr[largest]) 
        largest = l; 
  
    // If right child is larger than largest so far 
    if (r < n && arr[r] > arr[largest]) 
        largest = r; 
  
    // If largest is not root 
    if (largest != i) { 
        swap(arr[i], arr[largest]); 
  
        // Recursively heapify the affected sub-tree 
        heapify(arr, n, largest); 
    } 
} 
/*
 * No esta chequeado 
*/
void buildHeap(int arr[], int n) 
{ 
    // Index of last non-leaf node 
    int startIdx = (n / 2) - 1; 
  
    // Perform reverse level order traversal 
    // from last non-leaf node and heapify 
    // each node 
    for (int i = startIdx; i >= 0; i--) { 
        heapify(arr, n, i); 
    } 
} 
```

</details>
</br>

#### **Demostracion de la complejidad de Heapify:**

Hacer Heapify con sift-down sobre un vector(`O(n)`) es mas efeiciente para crear un heap que ingresar elemento a elemento con sift-up(`O(n logn)`).

![Complejidad de Heapify](Imagenes/Complejidad%20de%20heapify.png)

## **Métodos de Ordenamiento**

Se pretenden usar items con claves, así poder controlar el ordenamiento.
El objetivo es reorganizar los items de forma que sus claves estén ordenadas de acuerdo a una regla bien definida.
Su primera característica es el tiempo de ejecución. La segunda, la memoria utilizada.

**Hay dos tipos de ordenamiento:**

- **Interno**
  - Lo que se ordena entra en memoria.
  - Cualquier item puede ser accedido facilmente.
- **Externo (ya no se usan)**
  - Lo que se ordena está en disco o memoria secundaria.
  - Se accede a los ítems secuencialmente o en grandes bloques.

**También se pueden categorizar en:**

- **No Adaptativos**
  - Solo usan operaciones de comparación o intercambio: la secuencia de operaciones que realizan es independiente del orden de los datos.
  - Ejemplo: el método de inserción.
- **Adaptativos**
  - Aquellos que realizan diferentes secuencias de operaciones dependiendo de las salidas de las comparaciones.

**Por último, también se los puede categorizar en:**

- **Estables**
  - Se mantiene el orden relativo entre elementos con claves repetidas.
- **No estables**
  - No se mantiene el orden relativo entre elementos con claves repetidas.

### **Tipos de Ordenamientos:**

- Burbujeo.
- Burbujeo Mejorado.
- Selección.
- Inserción.
- [Merge Sort](#merge-sort)
- [Quick Sort](#quick-sort)
- [Bucket Sort](#bucket-sort)
- [Counting Sort](#counting-sort)
- [Radix Sort](#radix-sort)

### **Quick Sort**

---

Es un tipo de ordenamiento con el precepto de divide y conquista.

Utiliza una pila auxiliar, lo que le ayuda a tener una complejidad de, en promedio, `O(nlog(n))`.

**No es estable:** en el peor de los casos es $`O(n^2)`$.

Su proceso de partición reordena el arreglo con las siguientes condiciones:

- El elemento $`a[i]`$ está en su lugar definitivo en el arreglo para algún valor de `i`.

- Ninguno de los elementos $`a[1],...,a[i-1]`$ es mayor que a[i] y ninguno de los elementos en $`a[i+1] ... a[r]`$ es menor que $`a[i]`$. *Menor a mayor.*

![Quick Sort](Imagenes/quick_sort.gif)

#### **Pasos Quick Sort:**

1. Se elige un elemento de la lista de elementos al que se le llama pivote.
2. Se particiona la lista en tres listas:
   1. L1: que contiene todos los elementos de L menos v que sean menores o iguales que el pivote.
   2. L2: que contiene al pivote.
   3. L3: que contiene todos los elementos de L menos v que sean mayores o iguales que el pivote.
3. Se aplica la recursión sobre L1 y L3.
4. Se unen todas las soluciones que darían forma final a la lista finalmente ordenada.

![Quick Sort](Imagenes/quick_sort_animation.gif)

#### **Implementación Quick Sort:**

<details><summary>Implementación:</summary>

```c
void intercambiar(int* a, int* b) {
  int aux;

  aux = *a;
  *a = *b;
  *b = aux;
}

int particion (int* vector, int primero, int ultimo) {
  int pivote = vector[ultimo];// mayor como pivote
  int i = (primero - 1); // Posicion del primero

  for(int j = primero; j <= ultimo - 1; j++) {

    if (vector[j] <= pivote){ //el elemento es menor al pivote
  
      i++; //aumento la posición del pivote
      intercambiar(&vector[i], &vector[j]);
    }
  }
  intercambiar(&vector[i + 1], &vector[ultimo]); //pongo al pivote en su posición
  
  return (i + 1);//posición del pivote
}

void quick_sort(int* vector, int primero, int ultimo) {

  if(primero < ultimo) {

    int pivote = particion(vector, primero, ultimo);
    quick_sort(vector, primero, pivote - 1);
    quick_sort(vector, pivote + 1, ultimo);
  }
}

void imprimir_vector(int* vector, int tamanio) {
  int i;
  
  for(i=0; i < tamanio; i++)
    printf("%d ", vector[i]);
  printf("\n");
}

int main() {
  int vector[] = {10, 7, 8, 9, 1, 5};
  int tamanio = sizeof(vector)/sizeof(vector[0]);
  
  quick_sort(vector, 0, tamanio-1); //ordeno de menor a mayor
  printf("Vector ordenado: \n");
  imprimir_vector(vector, tamanio);
  
  return;
}
```

</details>
</br>

#### **Complejidad Quick Sort:**

[Explicacion de Khanacademy](https://es.khanacademy.org/computing/computer-science/algorithms/quick-sort/a/analysis-of-quicksort)

En el peor de los casos se comporta con una complejidad de $`O(n^2)`$: cuando la partición está más desbalanceada, es decir, la primer partición es de `(n − 1)` elementos. Sucede cuando el pivote elegido es el elemento más pequeño o más grande. Se van a hacer `(n − 1)` llamadas recursivas anidadas, si se realiza la suma de (n − 1) cuyo resultado es $`\frac{(n − 1)}{2}`$ por lo cual, en este escenario el Quicksort tiene una complejidad $`O(n^2)`$.

En un caso promedio en el cual el algoritmo genera particiones de $`\frac{n}{2}`$ el tiempo promedio de ordenar las sub-listas es $`O(\frac{n}{2})`$. Con este razonamiento se obtiene que la ecuación de recurrencia es:

TODO

### **Merge Sort**

---

Es un tipo de ordenamiento con el precepto de divide y conquista.

Divide al encontrar el número MEDIO de la posición a medio camino entre PRIMERO y ULTIMO (se redondea hacia abajo).

Vencer se realiza al ordenar de manera recursiva los subarreglos en cada uno de los dos subproblemas creados por el paso de dividir.

Combinar al mezclar los dos subarreglos ordenados de regreso en un solo subarreglo ordenado `array[PRIMERO, …, ULTIMO]`.

Siempre hace lo mismo, por eso su complejidad siempre es de `O(log(n))`, no hay mejor ni peor caso, es único.

#### **Pasos Merge Sort**

- Si la longitud de la lista es 0 o 1, entonces ya está ordenada.
- Sino:
  - Divido la lista desordenada en dos sublistas de aproximadamente la mitad del tamaño.
  - Ordeno cada sublista recursivamente aplicando el ordenamiento por mezcla.
  - Mezclo las dos sublistas en una sola lista ordenada.

![Merge Sort](Imagenes/merge-sort.png)

#### **Implementación Merge Sort:**

<details><summary>Implementación</summary>

```c
void mezclar(int *vector, int inicio, int medio, int fin) {
  int auxiliar[fin - inicio + 1]; // creo un vector auxiliar
  int i = inicio, j = medio + 1, k = 0;

  while((i <= medio) && (j <= fin)){
    
    if(vector[i] <= vector[j]){
    
      auxiliar[k] = vector[i];
      k+=1;
      i+=1;
    }
    else {
    
      auxiliar[k] = vector[j];
      k+=1;
      j+=1;
    }
  }

  while(i <= medio){// agrego lo que me queda del primero

    auxiliar[k] = vector[i];
    k+=1;
    i+=1;
  }
  while(j <= fin) {
  
    auxiliar[k] = vector[j];
    k+=1;
    j+=1;
  }
  for(i = inicio; i <= fin; i++) //reemplazo el original por el auxiliar
    vector[i] = auxiliar[i - inicio];
}

void merge_sort(int *vector, int inicio, int fin){

  if(inicio < fin) {

    int medio = (inicio + fin)/2;
    merge_sort(vector, inicio, medio);
    merge_sort(vector, medio+1, fin);
    mezclar(vector, inicio, medio, fin);
  }
}

int main(){
  int vector[] = {10, 7, 8, 9, 1, 5};
  int tamanio = sizeof(vector)/sizeof(vector[0]);

  merge_sort(vector, 0, tamanio-1);

  return 0;
}
```

</details>
</br>

### **Bucket Sort**

---

Es un tipo de ordenamiento no comparativo.
Se utiliza cuando los datos están en un rango uniforme de valores.

#### **Pasos Bucket Sort**

- Se crean N canastos vacíos.
- Recorro el vector original poniendo cada elemento en su canastito.
- Ordeno cada canastito para que no esté vacío.
- Visito los canastitos en orden y pongo todos los elementos en el vector.

![Bucket](Imagenes/Bucket%20Sort.png)

[Visualizador de Bucket Sort](https://algorithm-visualizer.org/divide-and-conquer/bucket-sort)

#### **Complejidad Bucket Sort**

En el peor de los casos se comporta como $`O(n^2)`$.

### **Counting Sort**

---

Es un tipo de ordenamiento no comparativo.
Se utiliza cuando los datos están en un rango del 0 al 9.

#### **Pasos Counting Sort:**

- Creo un vector auxiliar de 10 posiciones, vacío.
- Recorro el vector original y uso el vector auxiliar como contador de veces que haya un elemento en el original: si en el original tengo cuatro veces 3, en `aux[3] = 4`. ![Conting Sort Paso 1](Imagenes/Counting_sort_1.gif)
- A cada posición del auxiliar, le sumo lo que hay en la posición anterior.![Conting Sort Paso 2](Imagenes/counting_sort_2.png)
- Creo un tercer vector, igual al original, vacío: tengo en cuenta de que su primera posición para mi es un 1.
- Ahora, obtengo el vector ordenado. Por cada posición del vector original:
  - Guardo el `elemento = elemento`.
  - Busco en el auxiliar en ese elemento, es decir `auxiliar[original[i]] = auxiliar[elemento] = elemento_auxiliar`.
  - Voy a la posición del valor encontrado en el paso anterior del tercer vector, es decir `tercer_vector[elemento_auxiliar - 1]` y guardo el primer valor: elemento.
- Vuelvo a la posición de elemento_auxiliar, o sea, `auxiliar[elemento]` y le resto uno a su valor.

![Counting Sort Paso 3](Imagenes/counting_sort_3.gif)

### **Radix Sort**

---

Es un tipo de ordenamiento no comparativo.

Se ordena por dígitos: del menos significativo al más.

Se realiza con la ayuda de colas.

#### **Pasos Radix Sort:**

[Visualizador de Radix Sort](https://algorithm-visualizer.org/divide-and-conquer/radix-sort)

Creo un vector auxiliar de 10 posiciones, vacío, de tipo cola. El dígito significativo actual es la unidad.

- Recorro el vector original, y encolo cada elemento en la posición del auxiliar que corresponda según su dígito significativo correspondiente (se empieza con el menor).
- Recorro el vector auxiliar y desencolo cada posición (hasta que esté vacía), guardando los elementos en el vector original.
- Aumento el valor del dígito significativo actual. Si ya está ordenado, termino, sino, vuelvo a empezar.

![Radix Sort](Imagenes/radixsort.gif)

### **Heapsort**

![Heapsort](Imagenes/Heapsort.gif)

Heapsort es un ordenamiento in-place no estable que utiliza a la propiedad de heap para ordenar sus elementos.

1. Le damos froma de heap al arreglo(heapify) -> `O(n)`
2. Buscamos el maximo (arr[0]) -> (`O(1)`)
3. Swappeamos primero con ultimo -> (`O(1)`)
4. Reducimos la cantidad logica del arreglo y aplicamos sift-down a la raiz -> (`O(logn)`)
5. Si siguen quedando elementos sin ordenar volvemos al paso 2

**Complejidad:** `O(n logn)`

## **TDA Tabla de Hash**

Tabla hash es una estructura que ***almacena datos de forma asociativa***. Los datos se almacenan en un formato de arreglo, donde cada valor de datos tiene su propio valor de índice único. El acceso a los datos se vuelve muy rápido si conocemos el índice de los datos deseados.

La tabla hash almacena elementos en pares ***clave-valor*** donde:

- **Clave:** dato único que se utiliza para indexar los valores (la clave puede ser de cualquier tipo de dato siempre y cuando la función hash pueda interpretarla para generar un indice a partir de ella en la tabla).
- **Valor:** datos asociados a las claves.

Así, se convierte en una estructura de datos en la que ***las operaciones de inserción y búsqueda son muy rápidas independientemente del tamaño de los datos***.

El hash se utiliza para la implementación de lenguajes de programación, sistemas de archivos, búsqueda de patrones, almacenamiento distribuido de valores clave, criptografía, etc.

### **Funcion de Hash**

En una tabla hash, se procesa un nuevo índice utilizando las claves. Y el elemento correspondiente a esa clave se almacena en el índice. Este proceso se llama hashing.

> Sea **k** una clave y `h(x)` una función hash. Aquí, `h(k)` nos dará un nuevo índice para almacenar el elemento vinculado con **k**.

Una buena función hash debería tener las siguientes propiedades:

1. Eficientemente computable.
2. Debe distribuir uniformemente las claves (cada posición de la tabla es igualmente probable para cada clave).
3. La misma clave debe devolver lo mismo

**El método del módulo:**

En este método para crear funciones hash, se asigna una clave a una de los indices de la tabla tomando el resto de la clave dividida por el tamaño de la tabla. Si **k** es una clave y **m** es el tamaño de la tabla de hash,

$`h(k) = k \% m`$

La ventaja del método del modulo es que el hash siempre va a ser menor igual a **m**.

### **Colisión y sus resoluciones**

Cuando la función de hash genera el mismo índice para varias claves, habrá un conflicto (qué valor se almacenará en ese índice). A esto se le llama colisión de hash, no se pueden evitar, simplemente hay que manejar las colisiones.

Hay que tener en cuenta que una colisión solo se da cuando la función hash genera un indice que ya esta ocupado, si se trata de la misma clave se trata de actualización de valores.

> A misma clave mismo hash.

Podemos resolver la colisión utilizando una de las siguientes técnicas:

- ***Direccionamiento cerrado (Hash abierto):*** las claves se almacenan en listas enlazadas adjuntas a las celdas de una tabla hash.
- ***Direccionamiento abierto (Hash cerrado):*** todas las claves se almacenan en la propia tabla hash sin el uso de listas enlazadas.

El uso de "cerrado" frente a "abierto" refleja si estamos o no encerrados en el uso de un determinado hash o estructura de datos.

En resumen, "cerrado" siempre se refiere a algún tipo de garantía estricta, como cuando garantizamos que los objetos siempre se almacenan directamente dentro de la tabla hash. Entonces, lo opuesto a "cerrado" es "abierto", por lo que si no tiene tales garantías, la estrategia se considera "abierta".

#### **Hash Abierto, Direccionamiento cerrado:**

**Resolución por encadenamiento:**

En el encadenamiento, si una función de hash produce el mismo índice para varios elementos, estos elementos se almacenan en el mismo índice utilizando una [*lista enlazada*](#lista-enlazada).

![Hash abieto insertar](Imagenes/Hash_abierto_insertar.gif)

Si `k` es un indice para varios elementos, contiene un puntero al inicio de la lista de elementos. Si no hay ningún elemento presente, `k` contiene `NULL`. La idea es hacer que cada celda de la tabla apunte a una lista enlazada de elementos que tengan el mismo hash, es decir, la misma dirección en la tabla.

Si no tenemos suerte con las claves que generamos, o si tenemos una función hash mal implementada, todas las claves pueden usar el mismo hash. Esto significa que la complejidad en el peor de los casos es la misma que la de una lista enlazada: `O (n)` para insertar, buscar y eliminar.

#### **Direccionamiento abierto**

A diferencia del encadenamiento, el direccionamiento abierto no almacena varios elementos en un mismo indice. Aquí, cada espacio se llena con una sola clave o se deja en `NULL`.

> El tamaño de la tabla debe ser siempre mayor o igual a la cantidad de claves.

![Hash Cerrado](Imagenes/Hash%20Cerrado.gif)

##### **Diferentes técnicas de direccionamiento abierto para resolver una colisión:**

- <details><summary><b>Probing lineal</b></summary>

  Se busca linealmente un espacio en la tabla. Si `h(k)` es la función  hash,

  ```text
  Si el indice h(k) está ocupado, probamos (h(k) + 1)
  Si (h(k) + 1) también está ocupado, probamos (h(k) + 2)
  Si (h(k) + 1) también está ocupado, probamos (h(k) + 3)
  ..................................................
  ..................................................
  ```

  ![Probing Lineal](Imagenes/Hash%20Cerrado.png)

  Es decir, el indice aumenta linealmente hasta encontrar una celda vacía inmediata en la tabla para el nuevo elemento.

  El problema del probing lineal es que se llena un grupo de celdas   adyacentes. Al insertar un nuevo elemento, se debe atravesar todo el  clúster (grupo de elementos interrelacionados, es decir, con el mismo  hash). Esto se suma al tiempo necesario para realizar operaciones en la tabla hash.
  
  </details>
- <details><summary><b>Probing cuadratico</b></summary>
  Se busca la $`i ^ 2`$ celda en la iteración $`i`$. Si $`h(k)`$ es la función hash,

  ```text
  Si h(k) está ocupado, probamos (h(k) + 1*1)
  Si (h(k) + 1*1) también está ocupado, probamos (h(k) + 2*2)
  Si (h(k) + 2*2) también está ocupado, probamos (h(k) + 3*3)
  ..................................................
  ..................................................
  ```

  </details>
- <details><summary><b>Hash doble</b></summary>

  Usamos otra función de hash y buscamos la celda $`i * h_2(k)`$ en la  i-ésima iteración. Si $`h(k)`$ es la función hash,

  ```text
  Si h(k) está ocupado, probamos (h(k) + 1*h_2(k))
  Si (h(k) + 1*h_2(k)) también está ocupado, probamos (h(k) + 2*h_2(k))
  Si (h(k) + 2*h_2(k)) también está ocupado, probamos (h(k) + 3*h_2(k))
  ..................................................
  ..................................................
  ```

  </details>

###### **Problemas y solucion al borrar en un direccionamiento abierto:**

Puede pasar en el direccionamiento abierto que por borrar un elemento del hash no se encuentre otro que si esta debido a que este colisiono y se puso en otra posicion.

![Hash probing lineal insertar](Imagenes/Hash_linear_probing.png)

![Linear probing error al buscar](Imagenes/Hash_linear_probing_borrar_error.png)

Una solucion para esto es ponerle un flag de borrado a la posicion si un elemento fue borrado entonces:

- Cuando se esta buscando y se encuentra el flag, sigue buscando
- Si se quiere insertar en esa posicion se considera como vacia y el flag no molesta.

![Linear probing solucion al borrar](Imagenes/Hash_linear_probing_borrar_flags.png)

##### **Area de desbordamiento**

  Otro esquema dividirá la tabla preasignada en dos secciones: el área  principal a la que se asignan las claves y un área para colisiones,  normalmente denominada área de desbordamiento.

  Cuando ocurre una colisión, se usa una celda en el área de  desbordamiento para el nuevo elemento y un enlace desde la celda   primaria se establece como en un sistema encadenado. Esto es  esencialmente lo mismo que el encadenamiento, excepto que el área de  desbordamiento está preasignada y, por lo tanto, posiblemente sea más rápido de acceder.

  ![Hash area de desbordamiento](Imagenes/Hash%20cerrado%20area%20de%20desbordamiento.gif)

  Al igual que con el re-hash, el número máximo de elementos debe   conocerse de antemano, pero en este caso, deben estimarse dos   parámetros: el tamaño óptimo de las áreas primaria y de desbordamiento.

#### **Diferencias entre direccionamientos**

1. El encadenamiento es más sencillo de implementar. El direccionamiento abierto requiere más cálculos.
2. En el encadenamiento, la tabla de hash nunca se llena, siempre podemos agregar más elementos al encadenamiento. En el direccionamiento abierto, la tabla puede llenarse.
3. El encadenamiento es menos sensible a la función hash o los factores de carga. El direccionamiento abierto requiere un cuidado especial para evitar la agrupación y el factor de carga desfavorable.
4. El encadenamiento se usa principalmente cuando se desconoce cuántas claves y con qué frecuencia se pueden insertar o eliminar. El direccionamiento abierto se utiliza cuando se conoce la frecuencia y el número de claves.
5. El rendimiento de la caché del encadenamiento no es bueno ya que las claves se almacenan mediante una lista enlazada. El direccionamiento abierto proporciona un mejor rendimiento de la caché ya que todo se almacena en la misma tabla.
6. Desperdicio de espacio: Algunas partes de la tabla hash en el encadenamiento nunca se utilizan. En el direccionamiento abierto, se puede usar una celda incluso si una entrada no se asigna a ella.
7. El encadenamiento utiliza espacio adicional para los enlaces. El direccionamiento abierto no necesita espacio adicional.

### **Factor de carga**

Sea $`\alpha`$ el factor de carga, **n** el numero de claves almacenadas actualmente en la tabla hash y **m** la capacidad de la misma

$`\alpha = \frac{n}{m},\ 0 \leq \alpha \leq 1.`$

Este factor indica que tan ocupado esta el hash y que tan probable es que haya colisiones; debe mantenerse bajo, de modo que el número de entradas en un hash sea pequeño y, por lo tanto, la complejidad sea casi constante.

A medida que aumenta el factor de carga, la tabla hash se vuelve más lenta. La propiedad de tiempo constante esperada de una tabla hash supone que el factor de carga se mantiene por debajo de algún límite. Para un número fijo de celdas, el tiempo de búsqueda aumenta con el número de entradas y, por lo tanto, no se alcanza el tiempo constante deseado. En algunas implementaciones, la solución es aumentar automáticamente (generalmente, duplicar) el tamaño de la tabla cuando se alcanza el límite del factor de carga, lo que obliga a volver a realizar el hash en todas las entradas.

### **Rehash**

Significa hacer la tabla hash de nuevo. Básicamente, cuando el factor de carga aumenta a más de su valor predefinido (el valor predeterminado del factor de carga es 0,75), la complejidad aumenta. Entonces, para resolver esto, el tamaño del arreglo se aumenta (generalmente, se duplica) y a todos los valores se les vuelve a asignar un hash (un nuevo indice en el arreglo) y se almacenan en un nuevo arreglo de tamaño mayor para mantener un factor de carga bajo y una complejidad baja.

El rehashing se puede hacer en dos casos:

- Cuando la relación actual $`\frac{n}{m}`$ aumenta más allá del factor de carga
- La relación $`\frac{n}{m}`$ cae a un valor muy bajo, digamos 0.1.

#### **¿Cómo se hace el rehashing?**

El rehashing se puede hacer de la siguiente manera:

- Para cada adición de una nueva entrada a la tabla, hay que verificar el factor de carga.
- Si es mayor o igual que su valor predefinido (o el valor predeterminado de **0,75** si no se proporciona), entonces `rehash`.
- Para `rehash`, hay que armar un nuevo arreglo del doble del tamaño anterior.
- Luego, hay que recorrer cada elemento en el antiguo arreglo e insertarlo en el nuevo arreglo mas grande.

***Recordatorio:*** se pueden tener colisiones rehasheando. Rehashear no significa deshacerse de las colisiones.

### **Operaciones del Hash**

- ***Buscar:*** siempre que se busque un elemento, hay que calcular el indice de la tabla hash de la ***clave pasada*** y ubicar el elemento en el arreglo. Se puede utilizar probing lineal para adelantar el elemento si no se encuentra en el indice calculado. Se sigue buscando hasta que se encuentre el elemento buscado o se encuentre una celda vacía.
- ***Insertar:*** siempre que se inserte un elemento, hay que calcular el indice de la tabla hash de la ***clave pasada*** y ubicar el elemento en el arreglo. Si el indice calculado esta ocupado, se pueden utilizar uno de los métodos mencionados arriba para manejar la colisión.
- ***Eliminar:*** siempre que se elimine un elemento, si el hash es cerrado hay que asegurarse que todos los elementos puedan ser encontrados

## **Grafos**

La estructura de datos no lineal Gráfo es una colección de nodos que tienen datos y están conectados a otros nodos. Más precisamente, un gráfico es una estructura de datos `(V, E)` que consta de:

- Una colección de vértices `V` (o nodos).
- Una colección de aristas `E`, representadas como pares ordenados de vértices `(u, v)`.

### **Terminología de Grafos:**

- **Adyacencia:** Se dice que un vértice es adyacente a otro si hay una arista que los conecta.
- **Grafo no dirigido:** es un tipo de grafo en el cual las aristas representan relaciones simétricas y no tienen un sentido definido.

  ```mermaid
      graph LR
        C([V]) ---|Arista| D([V]);
  ```

- **Grafo dirigido:** o digrafo es un tipo de grafo en el cual las aristas tienen un sentido definido.
  
  ```mermaid
    graph LR
      A([V]) --> B([V]);
  ```

- **Peso:** Un gráfo es pesado si sus aristas tienen pesos asignados. El peso de la arista puede representar diferentes cosas según el problema (costos, distancias, capacidades, etc).
  
  ```mermaid
    graph LR
      A([V]) --->|30| B([V]);
      B ---> |14| A
  ```

- **Camino:**. Una secuencia de aristas que permite ir de un vértice a otro.
  - **Gráfo con ciclos:** Un gráfo tiene un ciclo cuando un camino de al menos dos aristas que empieza y termina en mismo vertice.
  - **Gráfo aciclico:** Un gráfo es aciclico cuando no tiene ciclos.
- **Gráfo conexo:** Un gráfo no dirigido es conexo si para cualquier par de vértices existe al menos un camino entre ellos.![Grafos conesox y no conexos](Imagenes/Grafo_conexo_y_no_conexo.png) *Izquierda conexo, derecho no*
  - **Vértices fuertemente conexos:** Un par de vértices ${A,B}$ son fuertemente conexos si existe un camino de A hacia B y otro de B hacia A.
  - **Vértices débilmente conexos:** Un par de vértices ${A,B}$ son débilmente conexos si existe un camino de A hacia B y otro de B hacia A, pero para lograr dichos caminos es necesario reemplazar una o mas aristas por aristas sin sentido.
  - **Dígrafo fuertemente conexo:** Un dígrafo es fuertemente conexo si todos los pares ${A,B}$ son fuertemente conexos.
  ![Digrafos conexos y no conesxos](Imagenes/Digrafos%20conexos%20y%20no%20conexos.png)
- **Árbol:** Un grafo es un árbol si es conexo y aciclico.
- **Orden:** El orden de un gráfo es la cantidad de vértices.
- **Tamaño:** El tamaño de un gráfo es la cantidad de aristas.
- **Grado:** El grado de un vértice es la cantidad de aristas incidentes. En un gráfo dirigido:
  - **Grado de entrada:** cantidad de aristas que entran al vertice.
  - **Grado de salida:** cantidad de aristas que salen del vertice.
- **Vacío:** Un gráfo es vacío si posee vértices pero no aristas.
- **Bucle:** Una arista es un bucle cuando conecta al vértice consigo mismo.
- **Gráfo simple:** Un gráfo es simple cuando no posee bucles ni aristas paralelas. Para un gráfo simple no dirigido,
  ![Grafos simples y no simples](Imagenes/grafos_no_simples.png)
- Para un gráfo simple no dirigido,**el indice dedensidad de un gráfo** se define como:
  $`D = \frac{2|E|}{|V|(|V|-1)}, \ 0 \leq D\leq 1`$
  - **V** es el número de nodos/vértices
  - **E** es el número de aristas.
  - Mas cercano a 1 mas **denso** es
  - Mas cercano a 0 mas **disperso** es.
- **Gráfo denso:** Un gráfo es denso cuando su numero de aristas esta muy cerca del valor maximo de aristas que este puede tener.
- **Gráfo disperso:** Un gráfo es disperso cuando su numero de aristas es bajo.
- **Gráfo completo:** Un gráfo es completo cuando posee todas las aristas posibles.

### **Representación de Grafos:**

La elección de la representación del gráfo es específica de la situación. Depende totalmente del tipo de operaciones que se realizarán y la facilidad de uso.

#### **Matriz de adyacencia:**

Una matriz de adyacencia es una matriz de $`V\ X\ V`$ vértices. Cada fila y columna representan un vértice. Si el valor de cualquier elemento $`A [i] [j]`$ es 1, representa que hay una arista conectando el vértice **i** y el vértice **j**.

- <details><summary><b>Grafo no Dirigido</b></summary>

  La matriz de adyacencia para el gráfo no dirigido es siempre simétrica (por ejemplo, para la arista (0,2), también hay que marcar la arista (2,0) haciendo la matriz de adyacencia simétrica sobre la diagonal), ya que $`A[i][j] = A[j][i]`$.

  ```mermaid
    graph LR
      A([A]) --- B
      B([B]) --- C
      B --- E
      C ---- E([E])
      C([C]) --- D([D])
  ```

  | |A|B|C|D|E|
  |-|-|-|-|-|-|
  |A|0|1|0|0|0|
  |B|1|0|1|0|1|
  |C|0|1|0|1|1|
  |D|0|0|1|0|0|
  |E|0|1|1|0|0|

  </details>
- <details><summary><b>Grafo Dirigido</b></summary>

  Si el gráfo fuera dirigido, la matriz solo registra al vértice de inicio y fin. $`A[i][j] = 1`$ si y solo si la arista parte del vértice $`i`$ hacia el vértice $`j`$.

  ```mermaid
    graph LR
      A([A]) ---> B
      B([B]) ---> C
      B ---> E
      C ----> E([E])
      C([C]) ---> D([D])
      D ---> C
  ```

  | |A|B|C|D|E|
  |-|-|-|-|-|-|
  |A|0|1|0|0|0|
  |B|0|0|1|0|1|
  |C|0|0|0|1|1|
  |D|0|0|1|0|0|
  |E|0|0|0|0|0|

  </details>
- <details><summary><b>Grafo Ponderados</b></summary>

  También se utiliza para representar gráfos con peso. Si $`A[i] [j] = W`$, entonces hay una arista de vértice $`i`$ a vértice $`j`$ con peso $`w`$.

  ```mermaid
    graph LR
      A([A]) --->|9| B
      B([B]) --->|2| C
      B --->|7| E
      C ---->|1| E([E])
      C([C]) --->|8| D([D])
      D --->|5| C
  ```

  | |A|B|C|D|E|
  |-|-|-|-|-|-|
  |A|0|9|0|0|0|
  |B|0|0|2|0|7|
  |C|0|0|0|8|1|
  |D|0|0|5|0|0|
  |E|0|0|0|0|0|

  </details>

##### **Ventajas y desventajas de las matrices de adyacencia**

- **Ventajas:**

  Las operaciones básicas como agregar una arista, eliminar una arista o verificar si hay una arista desde un vértice **i** a un vértice **j** son operaciones de tiempo constante `O(1)`.

  Si el gráfo es ***denso***, la matriz de adyacencia debe ser la primera opción. La representación es más fácil de implementar y seguir.

- **Desventajas:**

  El requisito de espacio $`V\ X\ V`$ de la matriz de adyacencia lo convierte en un problema cuando se trata de memoria, consume espacio `O (n^2)`. Mientras mas vertices tenga el gráfo mas grande la matriz,incluso si el gráfo es ***disperso***, consume el mismo espacio.

#### **Lista de adyacencia:**

Una lista de adyacencia representa un gráfo como un arreglo de listas vinculadas (ese arreglo, en vez de ser un arreglo, también puede ser otra lista y de cada nodo de la misma, salen otras listas). El tamaño del arreglo es igual al número de vértices. El índice del arreglo representa un vértice y cada elemento en su lista enlazada representa los otros vértices que forman una arista con el vértice.

**Representacion para:**

- <details><summary><b>Grafo no Dirigido</b></summary>

  ![Lista de adyacencia de un grafo no dirigido](Imagenes/Lista_de_adyacencias_grafo_no_dirigido.png)

  </details>
- <details><summary><b>Grafo Dirigido</b></summary>

  ![Lista de adyacencia de un grafo no dirigido](Imagenes/Lista_de_adyacencias_grafo_dirigido.png)

  </details>

Esta representación también se puede utilizar para representar un gráfo ponderado. Los pesos de las aristas se pueden representar como pares de listas.

##### **Ventajas y desventajas de las listas de adyacencia**

- **Ventajas:**

  Una lista de adyacencia es eficiente en términos de almacenamiento porque solo se necesitan almacenar los valores para las aristas. Para un gráfo con millones de vértices, esto puede significar mucho espacio de guardado.

  Usa espacio $`O (| V | + | E |)`$ en el caso de un gráfo dirigido, es menor al espacio que utiliza un grafo no dirigido ya que solo representa los vértices con dirección.

- **Desventajas:**

  Las consultas como si hay una arista de vértice **u** a vértice **v** no son eficientes y se pueden ser `O(n)`.

#### **Matriz de Incidencia:**

Es otra forma para representar un grafo, se denomina matriz de incidencia. En la cual las columnas son los vertices y las aristas son las filas:

<details><summary><b>Grafo no Dirigido Representacion:</b></summary>

![Matriz de Incidencia Grafo no Dirigido](Imagenes/matriz%20de%20incidencia%20grafo%20no%20dirigido.png)

</details><br>

Si el grafo a representar es dirigido, se utiliza la siguiente convencion, -1 se le asigna al vertice de salida y 1 al vertice de entrada:

<details><summary><b>Grafo Dirigido Representacion:</b></summary>

![Matriz de Incidencia Grafo no Dirigido](Imagenes/matriz%20de%20incidencia%20grafo%20dirigido.png)

</details><br>

En el caso que el grafo fuera dirigido y con peso, en vez de utilizar el numero 1 se utiliza el numero del peso de cada
arista.

El costo en espacio de esta representacion es **O(V ∗ A)**

### **Recorridos de grafos:**

---

El orden en el cual los vértices son visitados es la forma en que se clasifica el tipo de recorrido. Hay dos formas clásicas de recorrer un gráfo, en anchura o en profundidad.

Cuando se realiza un recorrido en un gráfo, el algoritmo tiene dos acciones bien definidas:

- **Visitar:** normalmente esta acción es marcar como visitado el nodo para evitar los ciclos.
- **Explorar:** es la política que va a definir como se comportara el algoritmo, explorar los vecinos, explorar los hijos, etc.

#### **Recorrido en profundidad (DFS):**

El algoritmo comienza en el vértice raíz (seleccionando algunos nodos arbitrarios como el vértice raíz en el caso de un gráfo) y explora lo más posible a lo largo de cada rama antes de retroceder. Por lo tanto, la idea básica es comenzar desde la raíz o cualquier vértice arbitrario, marcar el vértice como visitado y moverse al vértice adyacente no visitado. Se continúa con este bucle hasta que no haya mas vértice por visitar. Luego se retrocede, se verifica si hay otros vértice sin visitar y si los hay se recorren.

**Pasos:**

1. Se agrega el vertice en el tope de la pila de vertices visitados
2. Se marca como visitado
3. Se chequea si dicho vertice tiene hijos:
   1. Si los tiene se verifican que no hayan sido visitados y se visitan
   2. Si ha sido visitado se hace pop del nodo de la pila.

![DFS](Imagenes/DFS.gif)

<details><summary><b>Ejemplo con pasos de DFS</b></summary>

![DFS Pasos](Imagenes/DFS_pasos.gif)

|Paso  |             Imagen              |Descripcion|
|:----:|:-------------------------------:|-----------|
|  1   |![Paso 1 DFS](Imagenes/dfs_one.jpg)  |Inicializar la pila|
|  2   |![Paso 2 DFS](Imagenes/dfs_two.jpg)  |Marcamos **S** como visitado y lo agregamos a la pila. Despues elegimos uno de los 3 nodos adyacentes, en este caso **A** porque vamos en orden alfabetico|
|  3   |![Paso 3 DFS](Imagenes/dfs_three.jpg)|Marcamos A como visitado y lo ponemos en la Pila y exploramos el proximo nodo sin visitar, el **D**|
|  4   |![Paso 4 DFS](Imagenes/dfs_four.jpg) |Visitamos **D** lo marcamos como visitado y lo ponemos en la pila.Chequeamos los nodos adyacentes sin visitar **(B,C)** y elegimos **B** porque vamos en orden alfabetico.|
|  5   |![Paso 5 DFS](Imagenes/dfs_five.jpg) |Visitamos **B** lo marcamos como visitado y lo ponemos en la pila. Vemos que no hay nodo adyacentes sin visitar y desapilamos **B**|
|  6   |![Paso 6 DFS](Imagenes/dfs_six.jpg)  |Chequeamos el tope de la pila y chequeamos si quedan nodos sin visitar. Encontramos **D** y el unico nodo adyacente sin visitar es **C**|
|  7   |![Paso 7 DFS](Imagenes/dfs_seven.jpg) |Visitamos **C** y lo marcamos como visitado y lo agregamos a la pila. Ahora no quedan nodos sin vistar|

</details><br>

- La ***complejidad del tiempo*** del algoritmo se representa en forma de `O (V + E)`, donde:
  - **V** es el número de nodos/vértices
  - **E** es el número de aristas.

La ***complejidad del espacio del algoritmo*** es `O (V)`, dado que, se necesita un arreglo de vértices visitados adicional de tamaño V.

#### **Recorrido en ancho (BFS):**

El recorrido a lo ancho consiste en ir recorriendo el gráfo empezando desde un vértice cualquiera, y luego se van visitando los vértices adyacentes mas cercanos.

**Pasos:**

1. Agregar un nodo/vertice del grafo a la cola de nodos a ser visitados.
2. Visitar el primer nodo de la cola, y marcarlo como tal
3. Si el nodo tiene algun vecino, verificar si han sidos visitados o no.
4. Agregar a la colla todos aquellos vecinos que tienen que ser visitados a la cola.
5. Eliminar el nodo visitado de la cola

![BFS](Imagenes/BFS.gif)

<details><summary><b>Ejemplo con pasos de BFS</b></summary>

![BFS Pasos](Imagenes/BFS_pasos.gif)

|Paso  |             Imagen              |Descripcion|
|:----:|:-------------------------------:|-----------|
|  1   |![Paso 1 bfs](Imagenes/bfs_one.jpg)  |Inicializar la cola|
|  2   |![Paso 2 bfs](Imagenes/bfs_two.jpg)  |Arrancamos desde **S** y lo marcamos como visitado.|
|  3   |![Paso 3 bfs](Imagenes/bfs_three.jpg)|Encolamos **S** y visitamos sus nodos adyacentes sin visitar.Vamos alfabeticamente y elegimos **A**, lo marcamos como visitado y encolamos.|
|  4   |![Paso 4 bfs](Imagenes/bfs_four.jpg) |Despues visitamos el proximo nodo sin vistar de **S** que es **B**. Lo marcamos como visitado y lo encolasmos.|
|  5   |![Paso 5 bfs](Imagenes/bfs_five.jpg) |Despues visitamos el proximo nodo sin vistar de **S** que es **C**. Lo marcamos como visitado y lo encolasmos.|
|  6   |![Paso 6 bfs](Imagenes/bfs_six.jpg)  |Ahora, **S** no tiene nodos sin visitar. Entonces lo desencolamos y bamos a **A**.|
|  7   |![Paso 7 bfs](Imagenes/bfs_seven.jpg) |Desde **A** vemos que el proximo nodo adyacente sin visitar es **D*.Lo marcamos como visitado y lo encolamos.|

</details><br>

La ***complejidad del tiempo*** del algoritmo se representa en forma de `O (V + E)`, donde:

- **V** es el número de nodos/vértices
- **E** es el número de aristas.

La ***complejidad del espacio*** del algoritmo es `O (V)`.

### **Orden topológico**

---

La [clasificación topológica](https://youtu.be/plrfieT8BNc?t=7214) para un ***gráfo acíclico dirigido*** es una ordenación lineal de vértices tal que para cada arista dirigida `u - v`, el vértice **u** viene antes de **v** en la ordenación. No hay un único orden topológico.

### **Algoritmos de camino minimo:**

---

#### **Dijkstra**

![Dijkstra Demo](Imagenes/DijkstraDemo.gif)

El [algoritmo de Dijkstra](https://youtu.be/pu2e2N6Y4NA?t=1932) (algoritmo greedy) nos permite encontrar el camino más corto entre dos vértices de un gráfo.

La **idea del algoritmo de Dijkstra** consiste en ir explorando todos los caminos mas cortos que parten del vertice origen y que llevan a todos los demas vertices; cuando se **obtiene el camino mas corto desde el vertice origen hasta el resto de los vertices** que componen el grafo, el algoritmo se detiene. **No funciona** en grafos con aristas de **peso negativo**.

Djikstra usó esta propiedad en la dirección opuesta, es decir, sobreestimamos la distancia de cada vértice desde el vértice inicial. Luego visitamos cada nodo y sus vecinos para encontrar el subtrayecto más corto hacia esos vecinos.

**Los pasos para implementar el algoritmo de Dijkstra son los siguientes:**

1. Elegir un vértice para comenzar. La distancia del mismo sera 0 y la del resto de los vértices la inicializamos en ∞.
2. Calculo la distancia hacia los vértices adyacentes sumando los pesos de cada arista que los conecta con la distancia del vértice actual.
Si existen distancias previamente calculadas pero conozco una nueva que me lleve a un vértice ya conocido que sea menor, lo actualizo.
3. Marco el vértice actual como visitado y me muevo hacia el vértice con menor distancia conocida.
4. Repito hasta llegar al vértice objetivo.

![Dijkstra Pasos](Imagenes/Dijkstra_pasos.gif)

<details><summary><b>Ejemplo con pasos del Algoritmo de Dijkstra</b></summary>

![Dijkstra Grafo Ejemplo](Imagenes/Dijkstra_ejemplo.png)

- **Paso 1:**
  
  ![Dijkstra Tabla Paso 1](Imagenes/Dijkstra_tabla_1.png)
   
  Elegimos como el nodo de inicio **S**, armamos la tabla de nodos con su peso eninfinito y el nodo previo en NULL
- **Paso 2:**  
  
  ![Dijkstra Tabla Paso 2](Imagenes/Dijkstra_tabla_2.png)
  
  Nos fijamos los nodos adyacentes a **S** que son **A** y **B**:
    1. Chequemos primero para **A** su nueva distancia que es: `distancia_actual[S] + distancia[A] = 0 + 10 = 10` y `10 < ∞(Distacia Actual)`. Entonces actualizamos la distancia de **A** a 10 y ponemos **S** como nodo previo.
    2. Para **C**: `distancia_actual[S] + distancia[C] = 0 + 5 = 5` y `10 < ∞(Distacia Actual)`,actualizamos la distancia de **C** a 10 y ponemos **S** como nodo previo.
- **Paso 3:**
  
  ![Dijkstra Tabla Paso 3](Imagenes/Dijkstra_tabla_3.png)
  
  Ahora vamos al nodo **C** por ser el proximo nodo no visitado de menor distancia. De **C** tenemos 3 nodos adyacentes **A**,**B** y **C**:

  1. Para **A**: `distancia_actual[C] + distancia[A] = 5 + 3 = 8` y `8 < 10(Distacia Actual)`.Entonces actualizamos la distancia de **A** a 10 y ponemos **C** como nodo previo.
  2. Para **B**: `distancia_actual[C] + distancia[B] = 5 + 9 = 14` y `14 < ∞(Distacia Actual)`.Entonces actualizamos la distancia de **B** a 14 y ponemos **C** como nodo previo.
  3. Para **D**: `distancia_actual[C] + distancia[A] = 5 + 2 = 7` y `7 < ∞(Distacia Actual)`.Entonces actualizamos la distancia de **D** a 7 y ponemos **C** como nodo previo.
- **Paso 4:**
  
  ![Dijkstra Tabla Paso 4](Imagenes/Dijkstra_tabla_4.png)
  
  Ahora vamos al nodo **D** por ser el proximo nodo no visitado de menor distancia. De **D** tenemos 3 nodos adyacentes **S** y **B**:

  1. Para **S**: `distancia_actual[D] + distancia[S] = 7 + 7 = 14` y `14 > 0(Distacia Actual)`. No hacemos nada ya que la distancia actual es menor que la nueva calculada.
  2. Para **B**: `distancia_actual[D] + distancia[B] = 7 + 6 = 13` y `14 < ∞`.Entonces actualizamos la distancia de **B** a 13 y ponemos **D** como nodo previo.
- **Paso 5:**
  
  ![Dijkstra Tabla Paso 5](Imagenes/Dijkstra_tabla_5.png)
  
  Ahora vamos al nodo **A** por ser el proximo nodo no visitado de menor distancia. De **A** tenemos 3 nodos adyacentes **C** y **B**:

  1. Para **C**: `distancia_actual[A] + distancia[C] = 8 + 3 = 11` y `14 > 5(Distacia Actual)`. No hacemos nada ya que la distancia actual es menor que la nueva calculada.
  2. Para **B**: `distancia_actual[A] + distancia[B] = 8 + 1 = 9` y `9 < 13`.Entonces actualizamos la distancia de **B** a 9 y ponemos **A** como nodo previo.
- **Paso 6:**
  
  ![Dijkstra Tabla Paso 6](Imagenes/Dijkstra_tabla_6.png)
  
  Ahora solo queda **B** y su unco nodo adyacente es **D** y no puede actualizarlo porque la distancia es mayor

</details><br>

<details><summary><b>Pseudocodigo</b></summary>

Robado de wilipedia puede estar mal:

```pseudocode
     DIJKSTRA (Grafo G, nodo_fuente s)       
       para u ∈ V[G] hacer
           distancia[u] = INFINITO
           padre[u] = NULL
           visto[u] = false
       distancia[s] = 0
       adicionar (cola, (s, distancia[s]))
       mientras que cola no es vacía hacer
           u = extraer_mínimo(cola)
           visto[u] = true
           para todos v ∈ adyacencia[u] hacer
               si ¬ visto[v]      
                   si distancia[v] > distancia[u] + peso (u, v) hacer
                       distancia[v] = distancia[u] + peso (u, v)
                       padre[v] = u
                       adicionar(cola,(v, distancia[v]))
```

</details><br>

- ***Complejidad de tiempo:*** `O(E logV)` donde:
  - **V** es el número de nodos/vértices
  - **E** es el número de aristas.

#### **Floyd-Warshall**

El algoritmo Floyd-Warshall  (enfoque de programación dinámica) es un algoritmo para encontrar el camino más corto entre todos los pares de vértices de un gráfo ponderado. Este algoritmo funciona tanto para los gráfos ponderados dirigidos como para los no dirigidos.

Seguir los pasos a continuación para encontrar el camino más corto entre todos los pares de vértices:

1. Crear una matriz $`A_0`$ de dimensión $`n\ x \ n`$ donde **n** es el número de vértices. La fila y la columna están indexadas como **i** y **j** respectivamente. **i** y **j** son los vértices del gráfo.
Cada celda $`A [i] [j]`$ se rellena con la distancia desde el i-ésimo vértice hasta el j-ésimo vértice. Si no hay un ***camino directo*** desde el i-ésimo vértice al j-ésimo vértice, la celda se deja como infinito.
2. Ahora, crear una matriz $`A_1`$ usando la matriz $`A_0`$. Los elementos de la primera columna y la primera fila se dejan como están. Las celdas restantes se llenan de la siguiente manera.
Sea **k** el vértice intermedio en el camino más corto desde el origen al destino. $`A [i] [j]`$ se llena con $`(A [i] [k] + A [k] [j])$ si $(A [i] [j]> A [i] [k] + A [k] [ j])`$. Es decir, si la distancia directa desde la fuente al destino es mayor que el camino a través del vértice **k**, entonces la celda se rellena con $`A [i] [k] + A [k] [j]`$.
3. Del mismo modo, $`A_2`$ se crea utilizando $`A_3`$. Los elementos de la segunda columna y la segunda fila se dejan como están. Los pasos restantes son los mismos que en el paso
4. Repetir hasta terminar con todos los vértices.

<details><summary><b>Pseudocodigo</b></summary>

```pseudocode
let dist be a |V| × |V| array of minimum distances initialized to ∞ (infinity)

for each edge (u, v) do
    dist[u][v] ← w(u, v)  // The weight of the edge (u, v)
for each vertex v do
    dist[v][v] ← 0
for k from 1 to |V|
    for i from 1 to |V|
        for j from 1 to |V|
            if dist[i][j] > dist[i][k] + dist[k][j] 
                dist[i][j] ← dist[i][k] + dist[k][j]
            end if
```

</details><br>

<details><summary><b>Ejemplo del algoritmo de Floyd</b></summary>

![Ejemplo Floyd](Imagenes/Floyd_Warshall_ejemplo.png)

- **Con K=0**
  
  ```mermaid
    graph LR
      A([1]) -->|-2| B([3])
  ```

  ```mermaid
    graph LR
      A([2]) -->|4| B([1])
  ```

  ```mermaid
    graph LR
      A([2]) -->|3| B([3])
  ```

  ```mermaid
    graph LR
      A([3]) -->|2| B([4])
  ```

  ```mermaid
    graph LR
      A([4]) -->|-1| B([2])
  ```

  |  K=0  |j=1|j=2|j=3|j=4|
  |-------|---|---|---|---|
  |**i=1**| 0 | ∞ | −2| ∞ |
  |**i=2**| 4 | 0 | 3 | ∞ |
  |**i=3**| ∞ | ∞ | 0 | 2 |
  |**i=4**| ∞ | −1| ∞ | 0 |
- **Con K=1**
  
  ```mermaid
    graph LR
      A([2]) -->|4| B
      B([1]) -->|-2| C([3])
  ```

  |  K=1  |j=1|j=2| j=3 |j=4|
  |-------|---|---|-----|---|
  |**i=1**| 0 | ∞ |  −2 | ∞ |
  |**i=2**| 4 | 0 |**2**| ∞ |
  |**i=3**| ∞ | ∞ |  0  | 2 |
  |**i=4**| ∞ | −1|  ∞  | 0 |
- **Con K=2**

  ```mermaid
    graph LR
      A([4]) -->|-1| B
      B([2]) -->|4| C([1])
  ```

  ```mermaid
    graph LR
      A([4]) -->|-1| B
      B([2]) -->|4| C([1])
      C -->|-2| D([3])
  ```
  
  - Nuevo camino mas corto

  |  K=2  | j=1 |j=2| j=3 |j=4|
  |-------| --- |---| --- |---|
  |**i=1**|  0  | ∞ |  −2 | ∞ |
  |**i=2**|  4  | 0 |  2  | ∞ |
  |**i=3**|  ∞  | ∞ |  0  | 2 |
  |**i=4**|**3**| −1|**1**| 0 |
- **Con K=3**

  ```mermaid
    graph LR
      A([1]) -->|-2| B
      B([3]) -->|2| C([4])
  ```

  ```mermaid
    graph LR
      A([2]) -->|4| B
      B([1]) -->|-2| C([3])
      C -->|2| D([4])
  ```

  |  K=3  |j=1|j=2|j=3| j=4 |
  |-------|---|---|---|-----|
  |**i=1**| 0 | ∞ | −2|**0**|
  |**i=2**| 4 | 0 | 2 |**4**|
  |**i=3**| ∞ | ∞ | 0 |  2  |
  |**i=4**| 3 | −1| 1 |  0  |
- **Con K=4**
  
  ```mermaid
    graph LR
      A([3]) -->|2| B
      B([4]) -->|-1| C([2])
  ```

  ```mermaid
    graph LR
      A([3]) -->|2| B
      B([4]) -->|-1| C([2])
      C -->|4| D([1])
  ```

  ```mermaid
    graph LR
      A([1]) -->|-2| B
      B([3]) -->|2| C([2])
      C -->|-1| D([1])
  ```

  |  K=4  | j=1 |  j=2 |j=3|j=4|
  |-------| --- |------|---|---|
  |**i=1**|  0  |**-1**| −2| 0 |
  |**i=2**|  4  | 0    | 2 | 4 |
  |**i=3**|**5**|**1** | 0 | 2 |
  |**i=4**|  3  |  −1  | 1 | 0 |

</details><br>

- **Complejidad de tiempo:** $`O(n^3)`$.
  - **n** es el número de nodos/vértices

### **Algoritmos minimum spanning tree:**

---

Un algoritmo de árbol de expansión mínimo que toma un gráfico como entrada y encuentra el subconjunto de los bordes de ese gráfico que:

- Forman un árbol que incluya todos los vértices
- Tiene la suma mínima de pesos entre todos los árboles que se pueden formar a partir del gráfo.

#### **Algoritmo de Prim:**

![Prim Demo](Imagenes/PrimAlgDemo.gif)

El [algoritmo de Prim](https://youtu.be/pu2e2N6Y4NA?t=3995) ([algoritmo greedy](https://www.geeksforgeeks.org/greedy-algorithms/)) es un algoritmo de árbol de expansión mínimo.

Comenzando por un vértice arbitario, marcarlo como visitado e ir agregando la arista de menor peso que conecta un vértice ya visitado con uno no visitado (evita ciclos).

Los pasos para implementar el algoritmo de Prim son los siguientes:

1. Inicializar el árbol de expansión mínimo con un vértice elegido al azar.
2. Encontrar todas las aristas que conectan el vértice a nuevos vértices, elegir la mínima y agregarla al árbol. A medida que se agregan vértices al árbol se amplia la cantidad de aristas que podemos elegir, la única condición es que esa arista no lleve a un vértice ya agregado al arbol.
3. Seguir repitiendo el paso 2 hasta que obtengamos un árbol de expansión mínimo.

El algoritmo de Kruskal es otro algoritmo de árbol de expansión mínimo popular que usa una lógica diferente para encontrar el AEM de un gráfo. En lugar de comenzar desde un vértice, el algoritmo de Kruskal ordena todos las aristas de bajo peso a alto y sigue agregando las aristas más bajos, ignorando aquellas aristas que creen un ciclo.

<details><summary><b>Ejemplo con pasos del Algoritmo de Prim</b></summary>

|Imagen| Descripción| No visto |En el grafo| En el árbol|
|------|-------------|------------|-----------|------------|
|![Prim Paso 1](Imagenes/prim_1.png)| El vértice **D** ha sido elegido arbitrariamente como el punto de partida.| C, G| A, B, E, F| D|
|![Prim Paso 2](Imagenes/prim_2.png)| El segundo vértice es el más cercano a **D**: **A** está a 5 de distancia, **B** a 9, **E** a 15 y **F** a 6. De estos, 5 es el valor más pequeño, así que marcamos la arista DA.| C, G |B, E, F| A, D|
|![Prim Paso 3](Imagenes/prim_3.png)| El próximo vértice a elegir es el más cercano a D o A. B está a 9 de distancia de D y a 7 de A, E está a 15, y F está a 6. 6 es el valor más pequeño, así que marcamos el vértice F y a la arista DF.| C | B, E, G| A, D, F|
|![Prim Paso 4](Imagenes/prim_4.png)| El algoritmo continua. El vértice B, que está a una distancia de 7 de A, es el siguiente marcado. En este punto la arista DB es marcada en rojo porque sus dos extremos ya están en el árbol y por lo tanto no podrá ser utilizado.| null| C, E, G | A, D, F, B|
|![Prim Paso 5](Imagenes/prim_5.png)| Aquí hay que elegir entre C, E y G. C está a 8 de distancia de B, E está a 7 de distancia de B, y G está a 11 de distancia de F. E está más cerca, entonces marcamos el vértice E y la arista EB. Otras dos aristas fueron marcadas en rojo porque ambos vértices que unen fueron agregados al árbol.| null | C, G | A, D, F, B, E|
|![Prim Paso 6](Imagenes/prim_6.png)| Sólo quedan disponibles C y G. C está a 5 de distancia de E, y G a 9 de distancia de E. Se elige C, y se marca con el arco EC. El arco BC también se marca con rojo.| null | G | A, D, F, B, E, C|
|![Prim Paso 7](Imagenes/prim_7.png)| G es el único vértice pendiente, y está más cerca de E que de F, así que se agrega EG al árbol. Todos los vértices están ya marcados, el árbol de expansión mínimo se muestra en verde. En este caso con un peso de 39.| null | null | A, D, F, B, E, C, G|

</details><br>

<details><summary><b>Pseudocodigo</b></summary>

```pseudocode
   Prim (Grafo G)
       /* Inicializamos todos los nodos del grafo. 
       La distancia la ponemos a infinito y el padre de cada nodo a NULL
        Encolamos, en una cola de prioridad 
                  donde la prioridad es la distancia, 
               todas las parejas <nodo, distancia> del grafo*/
       por cada u en V[G] hacer
           distancia[u] = INFINITO
           padre[u] = NULL
           Añadir(cola,<u, distancia[u]>)
       distancia[u]=0
       Actualizar(cola,<u, distancia[u]>)
       mientras !esta_vacia(cola) hacer
           // OJO: Se entiende por mayor prioridad aquel nodo cuya distancia[u] es menor.
           u = extraer_minimo(cola) //devuelve el mínimo y lo elimina de la cola.
           por cada v adyacente a 'u' hacer
               si ((v ∈ cola) && (distancia[v] > peso(u, v)) entonces
                   padre[v] = u
                   distancia[v] = peso(u, v)
                   Actualizar(cola,<v, distancia[v]>)
```

</details><br>

- **Complejidad de tiempo:** `O(E logV)`.
  - **V** es el número de nodos/vértices
  - **E** es el número de aristas.

#### **Algoritmo de Kruskal:**

![Kruskal Demo](Imagenes/KruskalDemo.gif)

El [algoritmo de Kruskal](https://youtu.be/pu2e2N6Y4NA?t=4895) (algoritmo greedy) es un algoritmo de árbol de expansión mínimo

Se parte de las aristas con menor peso y se siguen agregando aristas hasta llegar al objetivo.

**Los pasos:**

1. Ordenar todos las aristas de bajo peso a alto.
2. Tomar la arista con el peso más bajo y agregarlo al árbol de expansión. Si agregar la arista crea un ciclo, rechazar esta arista.
3. Seguir agregando aristas hasta llegar a todos los vértices.

<details><summary><b>Ejemplo con pasos del Algoritmo de Kruskal</b></summary>

![Ejemplo Kruska](Imagenes/kruskal_ejemplo.jpg)

- **Paso 1:**
  Armamos la tabla de aristas ordenadas.

  |B_D|D_T|A_C|C_D|C_B|B_T|A_B|S_A|S_C|
  |:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
  | 2 | 2 | 3 | 3 | 4 | 5 | 6 | 7 | 8 |
- **Paso 2:**
  Empezamos a agregar las aristas de menor peso mientras que chequeamos que las propiedades de spanning siguen. En este caso se agregan **B_D** y **D_T**.

  |B_D|D_T|A_C|C_D|C_B|B_T|A_B|S_A|S_C|
  |:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
  |2✅|2✅| 3 | 3 | 4 | 5 | 6 | 7 | 8 |
  
  ![PASO 2](Imagenes/kruskal_ejemplo_1.jpg)
- **Paso 3:**
  Seguimos revisando las proximas aristas de menor peso: **A_C** y **C_D**. Ninguna froma un ciclo en el arbol entonces se agregan.

  |B_D|D_T|A_C|C_D|C_B|B_T|A_B|S_A|S_C|
  |:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
  |2✅|2✅|3✅|3✅| 4 | 5 | 6 | 7 | 8 |

  ![PASO 3](Imagenes/kruskal_ejemplo_2.jpg)
- **Paso 4:**
  Revisamos la proximas aristas de menor peso, **C_B** de 4. Esta arista formaria un ciclo en el arbol entonces no se agrega.
  
  |B_D|D_T|A_C|C_D|C_B|B_T|A_B|S_A|S_C|
  |:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
  |2✅|2✅|3✅|3✅|4❌| 5 | 6 | 7 | 8 |

  ![PASO 4](Imagenes/kruskal_ejemplo_3.jpg)
- **Paso 5:**
  Revisamos la proximas aristas de menor peso, **B_T** de 5 y **A_B** de 6. Esta arista formaria un ciclo en el arbol entonces no se agrega.

  |B_D|D_T|A_C|C_D|C_B|B_T|A_B|S_A|S_C|
  |:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
  |2✅|2✅|3✅|3✅|4❌|5❌|6❌|7 | 8 |

  ![PASO 5](Imagenes/kruskal_ejemplo_4.jpg)
- **Paso 6:**
  Revisamos la proximas aristas de menor peso, **S_A** de 7 y **S_C** de 8. Se agrega **S_A** primero al ser el menor y ahora se ignora a **S_C** porque formaria un ciclo en el arbol

  |B_D|D_T|A_C|C_D|C_B|B_T|A_B|S_A|S_C|
  |:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
  |2✅|2✅|3✅|3✅|4❌|5❌|6❌|7✅|8❌|

  ![PASO 6](Imagenes/kruskal_ejemplo_6.jpg)

</details><br>

![Kruskal Pasos](Imagenes/kruskal_pasos.gif)

- **Complejidad de tiempo:** `O(E logE)`.
  - **E** es el número de aristas.

## ♡ Visualizadores ♡

Ordenamientos, árboles, todo: [Visual Go](https://visualgo.net/en)

- se puede ingresar lo que uno quiera.

[algorithm-visualizer](https://algorithm-visualizer.org/) Bastante bueno

[Hacearth](https://www.hackerearth.com/practice/)

[ABB](https://www.cs.usfca.edu/~galles/visualization/BST.html)

[Arbol B](https://www.cs.usfca.edu/~galles/visualization/BTree.html)

[Rojo-Negro](https://www.cs.usfca.edu/~galles/visualization/RedBlack.html)

[Animaciones de Ordenamientos](https://www.toptal.com/developers/sorting-algorithms)
