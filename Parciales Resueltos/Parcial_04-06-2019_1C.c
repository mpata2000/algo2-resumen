/*Robado*/

#include <stdio.h>

1. Análisis de algoritmos

a. Sean:

 T1(n) = 3T(n/2) + n^2
 T2(n) = 4T(n/2) + n^2

Determinar y demostrar cuál de los dos algoritmos tiene mejor tiempo de ejecución.

--> Ecuacion 1: a = 3, b = 2, f(n) = n^2
	
	Parte recursiva del problema: log2(3) ~= 1.58496... ~= 2 --> 3T(n/2) ~= O(n^1.58496...)
	Entonces: O(n^1.58496...) < O(n^2) --> T1(n) = O(n^2)

--> Ecuacion 2: a = 4, b = 2, f(n) = n^2

	Parte recursiva del problema: log2(4) = 2 --> 4T(n/2) = O(n^2)
	Entonces: O(n^2) == O(n^2) --> T2(n) = O(n^2logn)

--> El algoritmo con ecuacion T1 tiene un mejor tiempo de ejecucion que el algoritmo con ecuacion T2.

b. Determinar el orden de crecimiento de la siguiente función:

void una_funcion(int n){

	int i, j, k, g, contador=0;

(1) for(i=0; i<=n; i++){ --> O(n + 1) --> O(n)

(2)		for(j=1; j<=n; j*=i) --> < O(logn)
			contador++;

		if (n%2==0)
(3)			for(g=n; g>=0; g--) --> O(n + 1) --> O(n)
				contador++;
		else
(4)			for(k=1; k<n; k*=2) --> O(logn)
				contador++;
	}
}

Del (3) y (4) for solo se ejecutara uno de los dos. Entonces, tomando el peor de los casos --> O(n) para esa seccion de codigo.
Los for (2), (3) y (4) no estan anidados, por ende, voy a decir que su complejidad es la maxima entre el for (2) y la declaracion 
if/else, max = O(n).
La complejidad del algoritmo es: T(n) = O(n)*O(n) = O(n^2)

2. Ordenamientos

a. Dado el siguiente vector: [a,c,d,g,e,r,y,t,f,h,z,c,b,n,m,w,u,i], ordenarlo ascendentemente mediante quicksort y mergesort mostrando 
todos los pasos intermedios.

Quick Sort:
	
(1) Elijo pivote: pivote = a y la posicion del pivote: pos_pivote = 17
(2) i > a? Si. Intercambio i con i: [a,c,d,g,e,r,y,t,f,h,z,c,b,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 16
(3) u > a? Si. Intercambio u con u: [a,c,d,g,e,r,y,t,f,h,z,c,b,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 15
(3) w > a? Si. Intercambio w con w: [a,c,d,g,e,r,y,t,f,h,z,c,b,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 14
...Esto se repite hasta finalizar el recorrido porque a es la menor letra del abecedario, por ende, quedara fija en ese lugar
(17) d > a? Si. Intercambio d con d: [a,c,d,g,e,r,y,t,f,h,z,c,b,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 1
(18) c > a? Si. Intercambio c con c: [a,c,d,g,e,r,y,t,f,h,z,c,b,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 0
(19) Termine el recorrido. Pongo el pivote en su lugar correspondiente (pos_pivote = 0) --> [a,c,d,g,e,r,y,t,f,h,z,c,b,n,m,w,u,i]

											[/*a*/,c,d,g,e,r,y,t,f,h,z,c,b,n,m,w,u,i]
											/									    \
										   -					    [c,d,g,e,r,y,t,f,h,z,c,b,n,m,w,u,i]

(20) Elijo pivote: pivote = c y la posicion del pivote: pos_pivote = 16
(21) i > c? Si. Intercambio i con i: [c,d,g,e,r,y,t,f,h,z,c,b,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 15
(22) u > c? Si. Intercambio u con u: [c,d,g,e,r,y,t,f,h,z,c,b,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 14
(23) w > c? Si. Intercambio w con w: [c,d,g,e,r,y,t,f,h,z,c,b,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 13
(24) m > c? Si. Intercambio m con m: [c,d,g,e,r,y,t,f,h,z,c,b,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 12
(25) n > c? Si. Intercambio n con n: [c,d,g,e,r,y,t,f,h,z,c,b,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 11
(26) b > c? No.
(27) c > c? No.
(28) z > c? Si. Intercambio z con b: [c,d,g,e,r,y,t,f,h,b,c,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 10
(29) h > c? Si. Intercambio h con c: [c,d,g,e,r,y,t,f,c,b,h,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 9
(30) f > c? Si. Intercambio f con b: [c,d,g,e,r,y,t,b,c,f,h,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 8
(31) t > c? Si. Intercambio t con c: [c,d,g,e,r,y,c,b,t,f,h,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 7
(32) y > c? Si. Intercambio y con b: [c,d,g,e,r,b,c,y,t,f,h,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 6
(33) r > c? Si. Intercambio r con c: [c,d,g,e,c,b,r,y,t,f,h,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 5
(34) e > c? Si. Intercambio e con b: [c,d,g,b,c,e,r,y,t,f,h,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 4
(35) g > c? Si. Intercambio g con b: [c,d,c,b,g,e,r,y,t,f,h,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 3
(35) d > c? Si. Intercambio d con b: [c,b,c,d,g,e,r,y,t,f,h,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 2
(36) Termine el recorrido. Pongo el pivote en su lugar correspondiente (pos_pivote = 2) --> [c,b,c,d,g,e,r,y,t,f,h,z,n,m,w,u,i]

											[/*a*/,c,d,g,e,r,y,t,f,h,z,c,b,n,m,w,u,i]
											/									    \
										   -						[c,b,/*c*/,d,g,e,r,y,t,f,h,z,n,m,w,u,i]
										   							/                                 \
										   					(37) [c,b]                (40) [d,g,e,r,y,t,f,h,z,n,m,w,u,i]

(37) Elijo pivote: pivote = c y la posicion del pivote: pos_pivote = 1
(38) b > c? No.
(39) Termine el recorrido. Pongo el pivote en su lugar correspondiente (pos_pivote = 1) --> [b,c]

(40) Elijo pivote: pivote = d y la posicion del pivote: pos_pivote = 13
(41) i > d? Si. Intercambio i con i: [d,g,e,r,y,t,f,h,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 12
(42) u > d? Si. Intercambio u con u: [d,g,e,r,y,t,f,h,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 11
(42) w > d? Si. Intercambio w con w: [d,g,e,r,y,t,f,h,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 10
(43) m > d? Si. Intercambio m con m: [d,g,e,r,y,t,f,h,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 9
(44) n > d? Si. Intercambio n con n: [d,g,e,r,y,t,f,h,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 8
...Esto se repite hasta finalizar el recorrido porque d es la menor letra del arreglo, por ende, quedara fija en ese lugar
(50) r > d? Si. Intercambio r con r: [d,g,e,r,y,t,f,h,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 2
(51) e > d? Si. Intercambio e con e: [d,g,e,r,y,t,f,h,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 1
(52) g > d? Si. Intercambio g con g: [d,g,e,r,y,t,f,h,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 0
(53) Termine el recorrido. Pongo el pivote en su lugar correspondiente (pos_pivote = 0) --> [d,g,e,r,y,t,f,h,z,n,m,w,u,i] 

						[/*a*/,c,d,g,e,r,y,t,f,h,z,c,b,n,m,w,u,i]
					    /									    \
					   -					[c,b,/*c*/,d,g,e,r,y,t,f,h,z,n,m,w,u,i]
										   	/                                     \
										[b,/*c*/]                [/*d*/,g,e,r,y,t,f,h,z,n,m,w,u,i]
										/                        /                               \
								    [/*b*/]                       -              (54) [g,e,r,y,t,f,h,z,n,m,w,u,i]

(54) Elijo pivote: pivote = g y la posicion del pivote = 12
(55) i > g? Si. Intercambio i con i: [g,e,r,y,t,f,h,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 11
(56) u > g? Si. Intercambio u con u: [g,e,r,y,t,f,h,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 10
(57) w > g? Si. Intercambio w con w: [g,e,r,y,t,f,h,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 9
(58) m > g? Si. Intercambio m con m: [g,e,r,y,t,f,h,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 8
(59) n > g? Si. Intercambio n con n: [g,e,r,y,t,f,h,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 7
(60) z > g? Si. Intercambio z con z: [g,e,r,y,t,f,h,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 6
(61) h > g? Si. Intercambio h con h: [g,e,r,y,t,f,h,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 5
(62) f > g? No.
(63) t > g? Si. Intercambio t con f: [g,e,r,y,f,t,h,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 4
(64) y > g? Si. Intercambio y con f: [g,e,r,f,y,t,h,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 3
(65) r > g? Si. Intercambio r con f: [g,e,f,r,y,t,h,z,n,m,w,u,i] y retrocedo la posicion del pivote: pos_pivote = 2
(66) e > g? No. 
(67) Termine el recorrido. Pongo el pivote en su lugar correspondiente (pos_pivote = 2) --> [f,e,g,r,y,t,h,z,n,m,w,u,i]

			[/*a*/,c,d,g,e,r,y,t,f,h,z,c,b,n,m,w,u,i]
		    /									    \
		   -					[c,b,/*c*/,d,g,e,r,y,t,f,h,z,n,m,w,u,i]
								/                                     \
							[b,/*c*/]                [/*d*/,g,e,r,y,t,f,h,z,n,m,w,u,i]
							/                        /                               \
						[/*b*/]                       -                    [f,e,/*g*/,r,y,t,h,z,n,m,w,u,i]
										                                 /                             \
										                         (68) [f,e]              (71) [r,y,t,h,z,n,m,w,u,i]

(68) Elijo pivote: pivote = f y la posicion del pivote: pos_pivote = 1
(69) e > f? No.
(70) Termine el recorrido. Pongo el pivote en su lugar correspondiente (pos_pivote = 1) --> [e,f]

(71) Elijo pivote: pivote = r y la posicion del pivote = 9
(72) i > r? No.
(73) u > r? Si. Intercambio u con i: [r,y,t,h,z,n,m,w,i,u] y retrocedo la posicion del pivote: pos_pivote = 8
(74) w > r? Si. Intercambio w con i: [r,y,t,h,z,n,m,i,w,u] y retrocedo la posicion del pivote: pos_pivote = 7
(75) m > r? No.
(76) n > r? No.
(77) z > r? Si. Intercambio z con i: [r,y,t,h,i,n,m,z,w,u] y retrocedo la posicion del pivote: pos_pivote = 6
(78) h > r? No.
(79) t > r? Si. Intercambio t con m: [r,y,m,h,i,n,t,z,w,u] y retrocedo la posicion del pivote: pos_pivote = 5
(80) y > r? Si. Intercambio y con n: [r,n,m,h,i,y,t,z,w,u] y retrocedo la posicion del pivote: pos_pivote = 4
(81) Termine el recorrido. Pongo el pivote en su lugar correspondiente (pos_pivote = 4) --> [i,n,m,h,r,y,t,z,w,u]

 [/*a*/,c,d,g,e,r,y,t,f,h,z,c,b,n,m,w,u,i]
 /									    \
-		             [c,b,/*c*/,d,g,e,r,y,t,f,h,z,n,m,w,u,i]
				     /                                     \
				[b,/*c*/]                [/*d*/,g,e,r,y,t,f,h,z,n,m,w,u,i]
				/                        /                               \
			[/*b*/]                       -                    [f,e,/*g*/,r,y,t,h,z,n,m,w,u,i]
										                     /                             \
										                [e,/*f*/]                [i,n,m,h,/*r*/,y,t,z,w,u]
										                /                        /                       \
										            [/*e*/]               (82) [i,n,m,h]           (86) [y,t,z,w,u]

(82) Elijo pivote: pivote = i y la posicion del pivote: pos_pivote = 3
(83) h > i? No.
(84) m > i? Si. Intercambio m con h: [i,n,h,m] y retrocedo la posicion del pivote: pos_pivote = 2 
(84) n > i? Si. Intercambio n con h: [i,h,n,m] y retrocedo la posicion del pivote: pos_pivote = 1
(85) Termine el recorrido. Pongo el pivote en su lugar correspondiente (pos_pivote = 1) --> [h,i,n,m]

(86) Elijo pivote: pivote = y y la posicion del pivote: pos_pivote = 4
(87) u > y? No. 
(88) w > y? No.
(89) z > y? Si. Intercambio z con u: [y,t,u,w,z] y retrocedo la posicion del pivote: pos_pivote = 3
(90) t > y? No.
(91) Termine el recorrido. Pongo el pivote en su lugar correspondiente (pos_pivote = 3) --> [w,t,u,y,z]

 [/*a*/,c,d,g,e,r,y,t,f,h,z,c,b,n,m,w,u,i]
 /									     \
-		             [c,b,/*c*/,d,g,e,r,y,t,f,h,z,n,m,w,u,i]
				     /                                     \
				[b,/*c*/]                [/*d*/,g,e,r,y,t,f,h,z,n,m,w,u,i]
				/                        /                               \
			  [b]                       -                    [f,e,/*g*/,r,y,t,h,z,n,m,w,u,i]
										                     /                             \
										                [e,/*f*/]                [i,n,m,h,/*r*/,y,t,z,w,u]
										                /                        /                       \
										              [e]                  [h,/*i*/,n,m]           [w,t,u,/*y*/,z]
										                				   /           \           /             \
										                				[/*h*/]     [m,/*n*/]  [u,t,/*w*/]     [/*z*/]
										                				   					   /
										                				   				  [t,/*u*/]
										                				   				  /
										                				   			  [/*t*/] 

--> Uniendo todas las soluciones: [a,b,c,c,d,e,f,g,h,i,m,n,r,t,u,w,y,z]

Merge Sort:                                 [a,c,d,g,e,r,y,t,f,h,z,c,b,n,m,w,u,i]
                                            /                                   \
                                  [a,c,d,g,e,r,y,t,f]                         [h,z,c,b,n,m,w,u,i]
                                  /                 \                         /                 \
                            [a,c,d,g,e]   		  [r,y,t,f]              [h,z,c,b,n]
                            /         \           /       \              /         \ 
                         [a,c,d]     [g,e]      [r,y]    [t,f]       [h,z,c]      [b,n]
                         /     \     /   \      /   \    /   \       /     \      /   \
                      [a,c]    [d] [g]   [e]  [r]   [y][t]   [f]  [h,z]    [c]   [b]  [n]
                      /   \                                       /   \
                    [a]   [c]                                   [h]   [z]

Merge-->                    			    [a,b,c,c,d,e,f,g,h,i,m,n,r,t,u,w,y,z]
                                            /                                   \
                                  [a,c,d,e,f,g,r,t,y]                         [b,c,h,i,m,n,u,w,z]
                                  /                 \                         /                 \
                            [a,c,d,e,f]   		  [f,r,t,y]              [b,c,h,n,z]            [i,m,u,w]
                            /         \           /       \              /         \            /       \
                         [a,c,d]     [e,g]      [r,y]    [f,t]       [c,h,z]      [b,n]      [m,w]     [i,u]
                         /     \     /   \      /   \    /   \       /     \      /   \      /   \     /   \
                      [a,c]    [d] [g]   [e]  [r]   [y][t]   [f]  [h,z]    [c]   [b]  [n]  [m]   [w] [u]   [i] 
                      /   \                                       /   \
                    [a]   [c]                                   [h]   [z]


3. Aritmética de punteros

a. ¿Cuál o cuáles de los siguientes algoritmos es/son correctos? Grafique cada uno.

/***** 1 *****/
int main(){     | STACK |
                |       |
	int a = 4;  | a = 4 |
	int* b;     | b     |
	b = &a;     | b-->a | 
}

/***** 2 *****/
int main(){		| STACK	|
				|       |
	int* b;     | b     |
	int a = 4;  | a = 4 | 
	b = &a;     | b-->a |
}

/***** 3 *****/
int main(){  
                
	int a = 4;  
	int* b;       
	*b = a; --> ERROR: no puedo leer *b. Su contenido es basura.     
}

/***** 4 *****/
int main(){ 
                 
	int* b;
	int a = 4;
	*b = a; --> ERROR: no puedo leer *b. Su contenido es basura. 
}

/***** 5 *****/
int main(){

	int a = 4;
	int* b;
	&b = a; --> ERROR: deberia ser b = &a. No le puedo asignar a a la direccion de b porque a no es un puntero y ademas la asignacion 
				esta al reves.
}

b. Dado el siguiente algoritmo, diagrame como se crean y llenan las estructuras. Libere la memoria reservada.

int main(){

	int*** matriz = malloc(3*sizeof(int**)); --> Reserva un bloque de memoria de tres punteros a int*.	
														|      STACK       |          HEAP          |
    													| int *** matriz --|--> [int**,int**,int**] |

	for(int i = 0; i < 3; i++)
		matriz[i] = malloc(3*sizeof(int*)); --> Reserva un bloque de memoria de tres punteros a int para cada puntero a int* del 
												bloque reservado antes.    
														|      STACK       |          HEAP          |
    													| int *** matriz --|--> [int**,int**,int**] |
                            							|				   |	   |	 |     |    |
                            							|				   |    [int*][int*][int*]	|
                            							|				   |    [int*][int*][int*]	|
                            							|				   |    [int*][int*][int*]	|
	for(int i = 0; i < 9; i++){ --> Con cada iteracion reserva un numero en el HEAP, primero es apuntado por numero que seria como la 
									la variable auxiliar para no perder la referencia. Luego asigna ese numero a uno de los punteros 
									de los bloques de memoria anteriores y al ingresar de nuevo a la proxima iteracion, numero ya no 
									va a estar apuntando a ese numero, si no que a otro nuevo. Finalmente, cada puntero de los tres 
									punteros de los tres bloques de memoria reservados en el primer for va a estar apuntando a un 
									numero (numeros del 1 al 9).

		int* numero = malloc(sizeof(int));
		*numero = 9-i;
		matriz[i/3][i%3] = numero;
	}

	for(int i = 0; i < 3; i++){

		for(int j = 0; j < 3; j++)
			free(matriz[i][j]); --> Libero los numeros

		free(matriz[i]); --> Libero un bloque apuntado por uno de los punteros de matriz
	}
	free(matriz); --> Libero la matriz

	return 0;
}

4. Recursividad

Cree una rutina recursiva que reciba una cola y dos pilas. Desencole los elementos y llene ambas pilas, una con los elementos al 
derecho y otra con los elementos al reves de como estan en la cola.

Asumo que tengo dos primitivas cola_vacia(), apilar() y desencolar() que:
- cola_vacia(): evalua si una COLA esta vacia o no.
- apilar(): apila en el tope de una PILA.
- desencolar(): desencola el primer elemento de una COLA.

COLA: [1,6,8,4,1,2] --> desencolo del principio --> desencolar(cola) --> [6,8,4,1,2]

void apilar_pilas_rec(cola_t cola, pila_t pila_1, pila_t pila_2){

	if(cola_vacia(cola)) return;

	void* elemento_desencolado = desencolar(cola); --> Guardo cada elemento que desencolo, los apilo en el stack (*)

	apilar(pila_1, elemento_desencolado);
	apilar_pilas_rec(cola, pila_1, pila_2);
	apilar(pila_2, elemento_desencolado); --> Hago uso de los elementos apilados en el stack para apilar al reves de como estan en la
											  COLA (*)
}

(*) Me van a quedar al final: [1,6,8,4,1,2] --> Pero cuando el stack comience a ir hacia atras, va a comenzar a desapilar por el 2.