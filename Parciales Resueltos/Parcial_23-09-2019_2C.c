#include <stdio.h>

1. Aritmética de punteros / Memoria Dinámica

En este juego de tronos tan cambiante, todos buscan obtener ventajas aún cuando sea traicionando a los demás y Daenerys Targaryen no 
perdona a quienes la traicionan, que han sido y serán muchos. Le llegó a sus manos un algoritmo que, según le afirmaron, tiene el 
nombre de alguien que la traicionó....

a. Realizar los diagramas del stack y del heap. Mostrar que se imprime por pantalla.
b. El algoritmo no libera la memoria, crear la parte que falta.

const​ unsigned int​ MAX_ABC = 26;
const​ int​ LETRA_A = 65;

int​ main​(){

	int​ pos_letras[9] = {3, 17, 14, 11, 18, 24, 17, 0, 21};

	/*abecedario = [A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z]*/
	char​* abecedario = malloc(sizeof(​char​)*MAX_ABC); --> Reservo un bloque de 26 bytes en el HEAP
	for​(int​ i = 0; i < MAX_ABC; i++)
		abecedario[i] = (​char​)(LETRA_A + i); --> Guarda el abecedario en el bloque anterior de memoria

	char​*** frase = malloc(sizeof(​char​**)*9); --> Reserva un bloque de memoria de punteros a char*
	for​(int​ i = 0; i < 9; i++){
		
		if​ (i < 4){
			
			frase[i] = malloc(sizeof(​char​*)*(size_t)(5-i)); --> Reserva memoria para un bloque de memoria de tamaño 5-i de punteros a 												  char
			for​(int​ j = 0; j < (5-i); j++)
				frase[i][j] = &(abecedario[pos_letras[i]]); --> Le asigna UNA de las letras del abecedario a cada una de las 													posiciones del bloque apuntado por uno de los punteros de frase
		} ​ 	
		else​{
			
			frase[i] = malloc(sizeof(​char​*)*(size_t)(i-3)); --> Reserva memoria para un bloque de memoria de tamaño i-3 de punteros a 												  char
			for​(int​ j = 0; j < (i-3); j++)
				frase[i][j] = &(abecedario[pos_letras[i]]); --> Le asigna UNA de las letras del abecedario a cada una de las 													posiciones del bloque apuntado por uno de los punteros de frase
		}
	}

	frase[0] tiene tamaño 5 --> frase[0] = [&abecedario[3], &abecedario[3], &abecedario[3], &abecedario[3], &abecedario[3]]
	frase[0] = [D,D,D,D,D]

	frase[1] tiene tamaño 4 --> frase[1] = [&abecedario[17], &abecedario[17], &abecedario[17], &abecedario[17]]
	frase[1] = [R,R,R,R]

	frase[2] tiene tamaño 3 --> frase[2] = [&abecedario[14], &abecedario[14], &abecedario[14]]
	frase[2] = [O,O,O]

	frase[3] tiene tamaño 2 --> frase[3] = [&abecedario[11], &abecedario[11]]
	frase[3] = [L,L]

	frase[4] tiene tamaño 1 --> frase[4] = [&abecedario[18]]
	frase[4] = [S]

	frase[5] tiene tamaño 2 --> frase[5] = [&abecedario[24], &abecedario[24]]
	frase[5] = [Y,Y]

	frase[6] tiene tamaño 3 --> frase[6] = [&abecedario[17], &abecedario[17], &abecedario[17]]
	frase[6] = [R,R,R]

	frase[7] tiene tamaño 4 --> frase[7] = [&abecedario[0], &abecedario[0], &abecedario[0], &abecedario[0]]
	frase[7] = [A,A,A,A]

	frase[8] tiene tamaño 5 --> frase[8] = [&abecedario[21], &abecedario[21], &abecedario[21], &abecedario[21],  &abecedario[21]]
	frase[8] = [V,V,V,V,V]

	for​(int​ i = 3; i >= 0; i--) printf(​ "%c"​ , frase[i][0][0]);

	i = 3 --> frase[3][0][0] = L, i = 2 --> frase[2][0][0] = O, i = 1 --> frase[1][0][0] = R, i = 0 --> frase[0][0][0] = D
	Salida primer for: LORD

	for​(int​ i = 8; i >= 4; i--) printf(​ "%c"​ , frase[i][0][0]);

	i = 8 --> frase[8][0][0] = v, i = 7 --> frase[7][0][0] = A, i = 6 --> frase[6][0][0] = R, i = 5 --> frase[5][0][0] = Y, 
	i = 4 --> frase[4][0][0] = S
	Salida primer for: VARYS

	--> Imprime LORD VARYS por pantalla

	for(int i = 0; i < 9; i++) free(frase[i]);
	free(frase);
	free(abecedario);

	return​ 0;
}

2. Análisis de Algoritmos (5 puntos)

a. El Teorema Maestro​ es el abc para hallar el tiempo de ejecución de algoritmos que utilizan divide y conquista, como herramienta de 
diseño de algoritmos:
	i. Enunciar el teorema.
	ii. Proveer una ecuación ejemplo para cada caso que presenta el teorema.

i. El TeoremaMaestro es una fórmula para resolver relaciones de recurrencia. Si a ≥ 1 y b > 1 son constantes y f(n) es una función 
asintóticamente positiva (una función asintóticamente positiva significa que para un valor suficientemente grande de n, tenemos 
f(n) > 0), entonces la complejidad de tiempo de una relación recursiva viene dada por: T(n) = aT(n/b) + f(n), donde, T(n) tiene los 
siguientes límites asintóticos:

    1. Si f(n) = O(n^(logb a-ϵ)), entonces T(n) = Θ(n^(logb a)).
    2. Si f(n) = Θ(n^(logb a)), entonces T(n) = Θ(n^(logb a)*logn).
    3. Si f(n) = Ω(n^(logb a+ϵ)), entonces T(n) = Θ(f(n)).
ϵ> 0 es una constante.

Cada una de las condiciones anteriores se puede interpretar como:
- Si el costo de resolver los subproblemas en cada nivel aumenta en un cierto factor, el valor de f(n) será polinomialmente menor que 
  n^(logb a). Por lo tanto, la complejidad del tiempo se ve oprimida por el costo del último nivel, es decir, n^(logb a).
- Si el costo de resolver el subproblema en cada nivel es casi igual, entonces el valor de f(n) será n^(logb a). Por tanto, la 
  complejidad temporal será f(n) veces el número total de niveles, es decir, n^(logb a)*logn.
- Si el costo de resolver los subproblemas en cada nivel disminuye en un cierto factor, el valor de f(n) será polinomialmente mayor 
  que n^(logb a). Por tanto, la complejidad del tiempo se ve oprimida por el coste de f(n).

Limitaciones del teorema maestro: el teorema maestro no se puede utilizar si...

- T(n) no es monótono. P.ej. T(n) = sen(n)
- f(n) no es un polinomio. P.ej. f(n) = 2n
- a no es una constante. P.ej. a = 2n
- a < 1

ii. Caso 1: T(n) = O(n) + O(1) = 2T(n/2) + 1 --> n^(logb a) > f(n) --> n > 1
	Caso 2: T(n) = O(n) + O(n) = 2T(n/2) + n --> n^(logb a) = f(n) --> n == n
	Caso 3: T(n) = O(n) + O(n^2) = 2T(n/2) + n^2 --> n^(logb a) < f(n) --> n < n^2 

b. Dadas las implementaciones con memoria dinámica de los tda pilas, colas y lista hacer un gráfico comparativo del tiempo de 
ejecución en notación Big-O de cada operación del tda, explique en qué implementación se basa para cada TDA. ¿Qué tda es el mejor?

IMPLEMENTACION:
- Lista: simplemente enlazada con un puntero al nodo inicial y un puntero al nodo final de la misma. Borrar borra el ultimo elemento 
de la lista e insertar inserta al final de la lista. Para borrar, mas alla de tener puntero al nodo final, necesito recorrerla para 
encontrar el anteultimo elemento de la lista y asignarle NULL como siguiente nodo.
- Pila: simplemente enlazada con un puntero al nodo final, el anterior del primer elemento de la pila es NULL (NULL <- nodo <- nodo 
... <- nodo final). Borrar desapila del tope de la pila e insertar apila en el tope de la misma. Unicamente se puede acceder al tope.
- Cola: simplemente enlazada con un puntero al nodo inicial y un puntero al nodo final de la misma. Borrar desencola el primer 
elemento de la cola e insertar encola en el tope de la misma. Unicamente se puede acceder al primer elemento y al tope.

|	OPERACION	|	PILA	|	COLA	|	LISTA	|	
-----------------------------------------------------
|crear		    |	O(1)	|	O(1)	|	O(1)	|
|insertar	    |	O(1)	|	O(1)    |	O(1)	|
|insertar_pos	|	 -	    |	 -	    |	O(n)	|
|borrar         |	O(1)	|	O(1)    |	O(n)	|
|borrar_pos	    |	 -	    |	 -	    |	O(n)	|
|destruir		|	O(n)	|	O(n)	|	O(n)	|

c. Calcular el tiempo de ejecución de:
i. T(n) = 3T (n/3) + n/2

   --> Parte no recursiva del problema: log3(3) = 1 --> 3T(n/3) = n
   --> f(n) = O(n) porque desprecio la constante 1/2
   --> f(n) es igual a n^(logb a) --> T(n) = O(nlogn)

ii. T (n) = 7T (n/2) + n​^2

   --> Parte no recursiva del problema: log2(7) ~= 2.80735... --> n^2.80735... ~= n^3
   --> f(n) es polinomicamente menor que n^(logb a) --> T(n) = O(n^3)

iii. T (n) = 64T (n/8) − n​^2 --> ecuacion invalida, tiene un - en vez de un +. No puede crecer negativamente f(n) (?)

Entre i e ii, el algoritmo con mejor tiempo de ejecucion es el i.

3. TDA

Westeros es, en superficie, equivalente a Sudamérica, a lo largo de su extensión podemos encontrar muchas casas​. Cada casa, tiene 
un nombre, un lema y un ejército. En estos tiempos de guerra, donde todo es dominación y traiciones, la geografía de Westeros cambia 
muy rápido. Dados los siguientes structs:

typedef struct casa{
	char​* nombre;
	char​* lema;
	int​ ejercito;
}casa_t;

typedef struct westeros{
	casa_t​* casas;
	int​ cantidad_casas;
}westeros_t;

Se pide implementar las siguientes funciones del tda westeros​:

#define EXITO 0
#define DOMINADA 0
#define ERROR -1
#define NO_DOMINADA -1
#define YA_EXISTE -2
#define NO_EXISTE -2

bool ya_existe_casa(westeros_t​* westeros, char* nombre){

	if(!westeros) return false;

	bool coincidencia = false; 

	for(int i = 0; i < westeros->cantidad_casas && !coincidencia; i++){

		if(strcmp(westeros->casas[i].nombre, nombre) == 0) coincidencia = true;
	}

	return coincidencia;
}

casa_t* crear_casa(char​* nombre, ​char​* lema, ​int​ ejercito){

	casa_t* nueva_casa = malloc(sizeof(casa_t));
	if(!nueva_casa) return NULL;

	strcpy(nueva_casa->nombre, nombre);
	strcpy(nueva_casa->lema, lema);
	nueva_casa->ejercito = ejercito;

	return nueva_casa;
}

int westeros_nueva_casa(westeros_t​* westeros, ​char​* nombre, ​char​* lema, ​int​ ejercito){

	if(!westeros) return ERROR;
	if(ya_existe_casa(westeros, nombre)) return YA_EXISTE;

	casa_t* casas_nuevo = (casa_t*)realloc(westeros->casas, sizeof(casa_t)*(size_t)(westeros->cantidad_casas+1));
	if(!casas_nuevo) return ERROR;
	
	casa_t* nueva_casa = inicializar_casa(nombre, lema, ejercito);
	if(!nueva_casa) return ERROR;

	westeros->casas = casas_nuevo;
	westeros->casas[cantidad_casas+1] = nueva_casa;
	(westeros->cantidad_casas)++;

	return EXITO;
}

bool no_existe_casa(westeros_t​* westeros, char* nombre_casa, int pos_casa){

	if(!westeros) return false;

	bool existe = false;

	for(int i = 0; i < westeros->cantidad_casas && !existe; i++){

		if(strcmp(westeros->casas[i].nombre, nombre_casa) == 0){

			existe = true;
			pos_casa = i;
		}
	}

	return existe;
}

bool westeros_achicado(westeros_t* westeros){

	if(!westeros​) return false;

	casa_t* casas_nuevo = (casa_t*)realloc(westeros->casas, sizeof(casa_t)*(size_t)(westeros->cantidad_casas-1));
	if(!casas_nuevo && westeros->cantidad_casas > 1) return false;

	westeros->casas = casas_nuevo;
	(westeros->cantidad_casas)--;

	return true;
}

void intercambiar_casas(casa_t* casas, int pos_eliminar, int tope){

	casa_t casa_aux = casas[pos_eliminar];
	casas[pos_eliminar] = casas[tope];
	casas[tope] = casa_aux;
}

bool dominada_exiliada(westeros_t* westeros, int pos_eliminar){

	if(pos_eliminar == westeros->cantidad_casas-1) return westeros_achicado(westeros);

	intercambiar_casas(westeros->casas, pos_eliminar, cantidad_casas-1);

	return westeros_achicado(westeros​);
}

/*
* La casa dominante intentará dominar a la casa dominada.
* Si ambas existen y el ejército de la dominante es mayor al de la dominada
* entonces la dominada deberá desaparecer, y la dominante aumentará
* su ejército a la mitad (porque muchos soldados mueren vio...).
* Si ambas existen y el ejército de la dominante es menor o igual al de la dominada
* entonces el ejército de la dominante bajará a la mitad (algunos huyen...).
* Se deberá liberar la memoria en caso de dominar la casa.
* Devolverá 0 si pudo dominarse la casa o -1 si no, si alguna de las casas no
* existe, devolverá -2.
*/
int westeros_dominar_casa(westeros_t​* westeros, char​* dominante, char​* dominada){

	if(!westeros) return ERROR; 

	int pos_dominante = 0, pos_dominada = 0;
	if(no_existe_casa(westeros, dominante, &pos_dominante) || no_existe_casa(westeros, dominada, &pos_dominada)) return NO_EXISTE;

	if(westeros->casas[pos_dominante]->ejercito > westeros->casas[pos_dominada]->ejercito){

		if(!dominada_exiliada(westeros, pos_dominada)) return ERROR;
		westeros->casas[pos_dominante] += (westeros->casas[pos_dominante])/2;
		return DOMINADA;
	}

	westeros->casas[pos_dominante] -= (westeros->casas[pos_dominante])/2;
	return NO_DOMINADA;
}

4. Recursividad

a. Dibujar el contenido de la pila luego de ejecutar algo​.
b. Indicar qué se imprime por pantalla.

#include <stdio.h>
#include ​ "pila.h"

void algo(char​* cosa, pila_t* pila){

	if​(*cosa == '\0') return; --> Este return ocurre cuando llamo a algo con cosa+1 = '\0'. Por ende, lo primero que se va a apilar
								  en el paso (2) es '\0' y luego todas las cosa+1 que hayan quedado apiladas en el stack de 
								  ejecucion, es decir, va a apilar en el orden inverso al que apilo hasta llegar al '\0'.
	
	pila_apilar(pila, cosa); (1)
	algo(cosa+1, pila);
	pila_apilar(pila, cosa+1); (2)
}

(1) Primero apila: pila = [?AB2LMOG, AB2LMOG, B2LMOG, 2LMOG, LMOG, MOG, OG, G]
(2) Luego apila: pila = [?AB2LMOG, AB2LMOG, B2LMOG, 2LMOG, LMOG, MOG, OG, G, \0, G, OG, MOG, LMOG, 2LMOG, B2LMOG, AB2LMOG] --> No se apila el ?AB2LMOG al final, porque nunca se hizo una llamada recursiva con ese string, en el stack de ejecucion se empezaron a apilar llamadas a algo con AB2LMOG.

int main(){

	pila_t* pila = pila_crear();
	char​* cosa = ​"?AB2LMOG"​;
	algo(cosa, pila);

	for​(int​ i=0; !pila_vacia(pila); i++){

		if​((i%3) == 0) ​//i es 0..3..6..9..etc
			printf(​"%c"​, *(char​*)pila_tope(pila));
		pila_desapilar(pila);
	}

	pila_destruir(pila);
	return​ 0;
}

i = 0 --> (char*)pila_tope(pila) == AB2LMOG --> *(char*)pila_tope(pila) = A
desapila: [?AB2LMOG, AB2LMOG, B2LMOG, 2LMOG, LMOG, MOG, OG, G, \0, G, OG, MOG, LMOG, 2LMOG, B2LMOG]

i = 1 --> desapila: [?AB2LMOG, AB2LMOG, B2LMOG, 2LMOG, LMOG, MOG, OG, G, \0, G, OG, MOG, LMOG, 2LMOG]
i = 2 --> desapila: [?AB2LMOG, AB2LMOG, B2LMOG, 2LMOG, LMOG, MOG, OG, G, \0, G, OG, MOG, LMOG]

i = 3 --> (char*)pila_tope(pila) == LMOG --> *(char*)pila_tope(pila) = L
desapila: [?AB2LMOG, AB2LMOG, B2LMOG, 2LMOG, LMOG, MOG, OG, G, \0, G, OG, MOG]

i = 4 --> desapila: [?AB2LMOG, AB2LMOG, B2LMOG, 2LMOG, LMOG, MOG, OG, G, \0, G, OG]
i = 5 --> desapila: [?AB2LMOG, AB2LMOG, B2LMOG, 2LMOG, LMOG, MOG, OG, G, \0, G]

i = 6 --> (char*)pila_tope(pila) == G --> *(char*)pila_tope(pila) = G
desapila: [?AB2LMOG, AB2LMOG, B2LMOG, 2LMOG, LMOG, MOG, OG, G, \0]

i = 7 --> desapila: [?AB2LMOG, AB2LMOG, B2LMOG, 2LMOG, LMOG, MOG, OG, G]
i = 8 --> desapila: [?AB2LMOG, AB2LMOG, B2LMOG, 2LMOG, LMOG, MOG, OG]

i = 9 --> (char*)pila_tope(pila) == OG --> *(char*)pila_tope(pila) = O
desapila: [?AB2LMOG, AB2LMOG, B2LMOG, 2LMOG, LMOG, MOG]

i = 10 --> desapila: [?AB2LMOG, AB2LMOG, B2LMOG, 2LMOG, LMOG]
i = 11 --> desapila: [?AB2LMOG, AB2LMOG, B2LMOG, 2LMOG]

i = 12 --> (char*)pila_tope(pila) == 2LMOG --> *(char*)pila_tope(pila) = 2
desapila: [?AB2LMOG, AB2LMOG, B2LMOG]

i = 13 --> desapila: [?AB2LMOG, AB2LMOG]
i = 14 --> desapila: [?AB2LMOG]

i = 15 --> (char*)pila_tope(pila) == ?AB2LMOG --> *(char*)pila_tope(pila) = ?
desapila: [] --> pila vacia

--> Salida: ALGO2?
