/*Robado*/

#include <stdio.h>
#include <stdbool.h>

1. Análisis de algoritmos

a. Sean:

 T1(n) =2T(n/2) + O(1)
 T2(n)= 2T(n/2) + O(n)

Determinar y demostrar cuál de los dos algoritmos tiene mejor tiempo de ejecución.

--> Ecuacion 1: a = 2, b = 2, f(n) = O(1)
	
	Parte recursiva del problema: log2(2) = 1 --> n^1 = n --> 2T(n/2) = O(n)
	.f(n) = O(1) es menor que O(n) --> T(n) = O(n)

--> Ecuacion 2: a = 2, b = 2, f(n) = O(n)
	
	Parte recursiva del problema: log2(2) = 1 --> n^1 = n --> 2T(n/2) = O(n)
	.f(n) = O(n) es igual que O(n) --> T(n) = O(nlogn)

--> O(n) < O(nlogn)
--> El algoritmo que tiene mejor tiempo de ejecucion es el algoritmo con ecuacion de recurrencia T1

b. Determinar el orden de crecimiento de la siguiente función:

void ejercicio_uno(int n){

	int i, j, k, contador=0; --> instruccion1
	for(i=0; i<=n; i++) (1)
		for(j=n ; j>=0; j--) (2)
			for(k=1; k<n; k*=2) (3)
				contador++; --> instruccion2
}

instruccion1 --> O(1)
(1) Se ejecuta n veces
(2) Se ejecuta n veces
(3) Se ejecuta logn veces aproximadamente --> por ejemplo, si n = 10, log(10) = 3.32192... por lo tanto se va a ejecutar 3 veces --> para k = 2, k = 4, k = 8 y cuando llegue a k = 10 va a dejar de iterar --> se realizan un total de 3 iteraciones
instruccion2 --> multiplicando los tiempos de ejecucion de cada for: O(n^2logn)

2. Ordenamientos

a. Dado el siguiente vector: [6,3,9,13,88,33,56,2,4,21,57]. Ordenarlo descendentemente mediante quicksort y mergesort mostrando todos 
los pasos intermedios.

Quick Sort:

(1) Elijo pivote: pivote = 57 y la posicion del pivote: pos_pivote = 0
(2) 6 > 57? No
(3) 3 > 57? No
(4) 9 > 57? No
(5) 13 > 57? No
(6) 88 > 57? Si. Intercambio 6 con 88 --> [88,3,9,13,6,33,56,2,4,21,57] y avanzo la posicion del pivote: pos_pivote = 1
(7) 33 > 57? No
(8) 56 > 57? No
(9) 2 > 57? No
(10) 4 > 57? No
(11) 21 > 57? No

(12) Termine el recorrido, intercambio el elemento que esta en pos_pivote con el del pivote --> [88,57,9,13,6,33,56,2,4,21,3]
(13) Divido mi problema: [88] - [9,13,6,33,56,2,4,21,3]. El [88] ya esta ordenado.

(14) Elijo pivote: pivote = 3 y la posicion del pivote: pos_pivote = 0
(15) 9 > 3? Si. Intercambio 9 con 9 --> [9,13,6,33,56,2,4,21,3] y avanzo la posicion del pivote: pos_pivote = 1
(16) 13 > 3? Si. Intercambio 13 con 13 --> [9,13,6,33,56,2,4,21,3] y avanzo la posicion del pivote: pos_pivote = 2
(17) 6 > 3? Si. Intercambio 6 con 6 --> [9,13,6,33,56,2,4,21,3] y avanzo la posicion del pivote: pos_pivote = 3
(18) 33 > 3? Si. Intercambio 33 con 33 --> [9,13,6,33,56,2,4,21,3] y avanzo la posicion del pivote: pos_pivote = 4
(19) 56 > 3? Si. Intercambio 54 con 56 --> [9,13,6,33,56,2,4,21,3] y avanzo la posicion del pivote: pos_pivote = 5
(20) 2 > 3? No
(21) 4 > 3? Si. Intercambio 4 con 2 --> [9,13,6,33,56,4,2,21,3] y avanzo la posicion del pivote: pos_pivote = 6
(22) 21 > 3? Si. Intercambio 21 con 2 --> [9,13,6,33,56,21,4,2,3] y avanzo la posicion del pivote: pos_pivote = 7

(23) Termine el recorrido, intercambio el elemento que esta en pos_pivote con el del pivote --> [9,13,6,33,56,21,4,3,2]
(24) Divido mi problema: [9,13,6,33,56,21,4] - [2]. El [2] ya esta ordenado.

(25) Elijo pivote: pivote = 4 y la posicion del pivote: pos_pivote = 0
(26) 9 > 4? Si. Intercambio 9 con 9 --> [9,13,6,33,56,21,4] y avanzo la posicion del pivote: pos_pivote = 1
(27) 13 > 4? Si. Intercambio 13 con 13 --> [9,13,6,33,56,21,4] y avanzo la posicion del pivote: pos_pivote = 2
(28) 6 > 4? Si. Intercambio 6 con 6 --> [9,13,6,33,56,21,4] y avanzo la posicion del pivote: pos_pivote = 3
(29) 33 > 4? Si. Intercambio 33 con 33 --> [9,13,6,33,56,21,4] y avanzo la posicion del pivote: pos_pivote = 4
(30) 56 > 4? Si. Intercambio 56 con 56 --> [9,13,6,33,56,21,4] y avanzo la posicion del pivote: pos_pivote = 5
(31) 21 > 4? Si. Intercambio 21 con 21 --> [9,13,6,33,56,21,4] y avanzo la posicion del pivote: pos_pivote = 6

(32) Termine el recorrido, intercambio el elemento que esta en pos_pivote con el del pivote --> [9,13,6,33,56,21,4]
(33) Divido mi problema: [9,13,6,33,56,21]

(34) Elijo pivote: pivote = 21 y la posicion del pivote: pos_pivote = 0
(35) 9 > 21? No
(36) 13 > 21? No
(37) 6 > 21? No
(38) 33 > 21? Si. Intercambio 33 con 9 --> [33,13,6,9,56,21] y avanzo la posicion del pivote: pos_pivote = 1
(39) 56 > 21? Si. Intercambio 56 con 13 --> [33,56,6,9,13,21] y avanzo la posicion del pivote: pos_pivote = 2

(40) Termine el recorrido, intercambio el elemento que esta en pos_pivote con el del pivote --> [33,56,21,9,13,6]
(41) Divido mi problema: [33,56] - [9,13,6]*

(42) Elijo pivote: pivote = 56 y la posicion del pivote: pos_pivote = 0
(43) 33 > 56? No

(44) Termine el recorrido, intercambio el elemento que esta en pos_pivote con el del pivote --> [56,33]
(45) Divido mi problema: [33]. El [33] ya esta ordenado.

*(46) Elijo pivote: pivote = 6 y la posicion del pivote: pos_pivote = 0
(47) 9 > 6? Si. Intercambio 9 con 9 --> [9,13,6] y avanzo la posicion del pivote: pos_pivote = 1
(48) 13 > 6? Si. Intercambio 13 con 13 --> [9,13,6] y avanzo la posicion del pivote: pos_pivote = 2

(49) Termine el recorrido, intercambio el elemento que esta en pos_pivote con el del pivote --> [9,13,6]
(50) Divido mi problema: [9,13]

(51) Elijo pivote: pivote = 13 y la posicion del pivote: pos_pivote = 0
(52) 9 > 13? No

(49) Termine el recorrido, intercambio el elemento que esta en pos_pivote con el del pivote --> [13,9]
(50) Divido mi problema: [9]. El [9] ya esta ordenado

--> Uniendo soluciones: 

					[88,/*57*/,9,13,6,33,56,2,4,21,3]
					/      \
				[88]		[9,13,6,33,56,2,4,21,3]
							[9,13,6,33,56,21,4,/*3*/,2]
											  /  \
							[9,13,6,33,56,21,4] - [2]
							[9,13,6,33,56,21,/*4*/]
											/
							[9,13,6,33,56,21]
							[33,56,/*21*/,9,13,6]
								/        \
							[33,56]       [9,13,6]
							[/*56*/,33]   [9,13,/*6*/]
								/          /  
							  [33]       [9,13]
							  		     [/*13*/,9]
							  		      /
							  		     [9]

--> [88,56,33,21,13,9,6,4,3,2]

Merge Sort: 

						[6,3,9,13,88,33,56,2,4,21,57]
						/                           \
			j	[6,3,9,13,88,33]               [56,2,4,21,57]
				/              \                /          \
		h  [6,3,9]        [13,88,33]   i  [56,2,4]       [21,57]
			/   \           /      \        /    \        /   \
	  d [6,3]   [9]	 e  [13,88]   [33] f [56,2]  [4]  g [21]  [57]
		/  \             /  \             /  \
	a [6]  [3]       b [13] [88]      c [56] [2]

Cuando se lleguen a subarreglos de 1 elementos, van a comenzar a fusionarlos:
a. merge: [6] con [3] --> [3,6]
b. merge: [13] con [88] --> [13,88]
c. merge: [56] con [2] --> [2,56]

d. merge: [3,6] (ordenado en el paso a) con [9] --> [3,6,9]
e. merge: [13,88] (ordenado en el paso b) con [33] --> [13,33,88]
f. merge: [2,56] (ordenado en el paso c) con [4] --> [2,4,56]
g. merge: [21] con [57] --> [21,57]

h. merge: [3,6,9] (ordenado en el paso d) con [13,33,88] (ordenado en el paso e) --> [3,6,9,13,33,88]
i. merge: [2,4,56] (ordenado en el paso f) con [21,57] (ordenado en el paso g) --> [2,4,21,56,57]

j. merge: [3,6,9,13,33,88] (ordenado en el paso h) con [2,4,21,56,57] (ordenado en el paso i) --> [2,3,4,6,9,13,21,33,56,57,88]

Merge lo que hace es fusionar ordenadamente dos vectores que recibe. Los recorre a la par y va comparando los elementos de un 
determinado indice de cada vector y dependiendo de la comparacion se van posicionando en sus lugares correctos.

3. Aritmética de punteros

a. Realice un diagrama del stack y del heap y como varían los datos y la memoria con la ejecución del programa. ¿Qué se imprime por 
pantalla?  --> Hecho en hoja. 
b. ¿El programa pierde memoria? En caso afirmativo, cuanta y como lo arreglaría. En caso negativo, muestre en su diagrama donde fue 
liberada toda la memoria reservada.

const size_t MAX_VECTOR = 5;

int main(){
	
	char*** frase = malloc(MAX_VECTOR*sizeof(char**));
	char letra = 'P';
	
	for(size_t i = 0; i < MAX_VECTOR; i++){

		frase[i] = malloc((i+1)*sizeof(char*));
		char* p_letra = malloc(sizeof(char));
		*p_letra = letra;
		
		for (size_t j = 0; j < i+1; j++){
			frase[i][j] = p_letra;
		}
		
		letra++;
	}
	
	*(frase[0][0]) = 'A';
	*(frase[1][0]) = *(frase[0][0]) + 10;
	printf("%c%c%c%c%c\n", frase[3][0][0], frase[4][0][0], frase[0][0][0], frase[2][0][0], frase[1][0][0]);

	for(size_t i = 0; i < MAX_VECTOR; i++){
		free(frase[i]);
	}
	free(frase);
	
	return 0;
}

a. Se imprime STARK por pantalla.
b. El programa pierde memoria porque reserva memoria en el heap para 5 letras y al liberar previamente los bloques de memoria que
apuntan a esas letras pierdo la referencia a las mismas. Por lo tanto estoy perdiendo 5 bytes de memoria, 1 bytes por cada letra.

Para liberarla haria lo siguiente:

for(size_t i = 0; i < MAX_VECTOR; i++){
	free(frase[i][0]); --> Aca recorro la flecha que apunta a la letra y la libero
	free(frase[i]); --> Luego libero el bloque que apuntaba a la letra
}

4. Recursividad

Sea una pila_t un TDA pila que almacena enteros y cuenta con las operaciones apilar, desapilar y vacía. Y sea invertir_y_mostrar un 
procedimiento que imprime los elementos de la pila al revés, se pide encontrar un algoritmo, que reciba los parámetros que crea 
convenientes y cumpla el mismo objetivo que invertir_y_mostrar, pero de manera recursiva. 
Aclaración: El desapilar, además de sacarlo de la pila, devuelve el elemento.

void invertir_y_mostrar(pila_t pila){

	pila_t pila_auxiliar;

	while(!vacia(pila))
		apilar(pila_auxiliar, desapilar(pila)); --> los guarda del ultimo al primero en una pila auxiliar o sea invertidos

	while(!vacia(pila_auxiliar))
		printf(“%i\n”, desapilar(pila_auxiliar)); --> los imprime
}

void invertir_y_mostrar_rec(pila_t pila){

	if(vacia(pila)) return;

	int elemento = desapilar(pila);
	invertir_y_mostrar();
	printf("%i", elemento);
}